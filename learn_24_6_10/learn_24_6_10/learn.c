#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	char arr[31] = { 0 };
//	scanf("%s", arr);
//	int i = 0;
//	while (arr[i])
//	{
//		if (islower(arr[i]))
//		{
//			arr[i] = toupper(arr[i]);
//		}
//		else if (isupper(arr[i]))
//		{
//			arr[i] = tolower(arr[i] );
//		}
//		i++;
//	}
//	printf("%s", arr);
//	return 0;
//}

//#include<stdio.h>
//void Swap(int *pa, int *pb)
//{
//	int tmp = *pa;
//	*pa = *pb;
//	*pb = tmp;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	Swap(&a, &b);
//	printf("%d  %d", a, b);
//	return 0;
//}


// 求两个数的平均值

//#include<stdio.h>
//int average(int x, int y)
//{
//	//return (x + y) / 2;
//	return x + (y - x) / 2;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int ret = average(a, b);
//	printf("ret = %d", ret);
//	return 0;
//}

//
//#include<stdio.h>
//int my_strlen(const char* str)
//{
//	int count = 0;
//	while (*str)
//	{
//		str++;
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char str[] = "hello world";
//	int ret = my_strlen(str);
//	printf("%d", ret);
//	return 0;
//}


// 字符串逆序
//#include<stdio.h>
//#include<string.h>
//void reverse(char *str)
//{
//	int len = strlen(str);
//	char * left = str;
//	char * right = str + len - 1;
//	while (left <= right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char str[] = "hello world";
//	reverse(str);
//	printf("%s", str);
//	return 0;
//}


// 求每位数之和
#include<stdio.h>
int digit_num(int x)
{
	int count = 0;
	while (x)
	{
		count += x % 10;
		x /= 10;
	}
	return count;
}
int main()
{
	int num = 1234567;
	int ret = digit_num(num);
	printf("ret = %d", ret);
	return 0;
}