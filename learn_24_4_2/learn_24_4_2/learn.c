#define _CRT_SECURE_NO_WARNINGS


#include<stdio.h>
int main() {

	return 0;
}



#if 0
#include<stdio.h>
#include<stdlib.h>
int main()
{
	int** arr = (int**)malloc(sizeof(int*) * 3);
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		arr[i] = (int*)malloc(sizeof(int) * 5);
	}
	int j = 0;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 5; j++)
		{
			arr[i][j] = 1;
		}
	}
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 5; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	for (i = 0; i < 3; i++)
	{
		free(arr[i]);
		arr[i] = NULL;
	}
	free(arr);
	arr = NULL;
	return 0;
}

#define SWAP(n) ((( (n) & 0x55555555) << 1)|((( (n) & 0xaaaaaaaa) >> 1)))
#include<stdio.h>
int main()
{
	printf("%d\n", SWAP(11));
	return 0;
}

#define A 2+2
#define B 3+3
#define C A*B
int main()
{
	printf("%d\n", C);
	return 0;
}


#include<stdio.h>
#define my_offsetof(s,m)((size_t)&(((s*)0)->m))
struct S
{
	char m;
	int n;
};
int main()
{
	printf("%d\n", my_offsetof(struct S, m));
	printf("%d\n", my_offsetof(struct S, n));
	return 0;
}


#include<stdio.h>
#define my_offsetof(s,m)((size_t)&(((s*)0)->m))
struct S
{
	char a;
	int b;
	double c;
};
int main()
{
	printf("%d\n", my_offsetof(struct S, a));
	printf("%d\n", my_offsetof(struct S, b));
	printf("%d\n", my_offsetof(struct S, c));
	return 0;
}





#include<stdio.h>
int main()
{
	printf("hello""world\n");
	printf("helloworld\n");
	return 0;
}


#define Print(n,format) printf("the value of " #n " is " format"\n",n)
#include<stdio.h>
int main()
{
	int a = 2;
	Print(a, "%d");
	return 0;
}


#define GENERIC_MAX(type) \
type type##_max(type x, type y)\
{ \
return (x>y?x:y); \
}

GENERIC_MAX(int);
GENERIC_MAX(float);
#include<stdio.h>
int main()
{
	int ret1 = int_max(4, 8);
	printf("ret1 = %d\n", ret1);
	float ret2 = float_max(3.14, 5.24);
	printf("ret2 = %f\n", ret2);
	return 0;
}
#endif