#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int i;
//void prt()
//{
//	for (i = 5; i < 8; i++)
//		printf("%c", '*');
//	printf("\t");
//}
//int main()
//{
//	for (i = 5; i <= 8; i++)
//		prt();
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 3;
//	printf("%d\n", (a += a -= a * a));
//	// a += b  b = a -= a * a = -6 
//	//a += b = -12
//	//赋值运算的右结合性
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int num = n * n - n + 1;
//    printf("%d", num);
//    for (int i = 1; i < n; i++)
//    {
//        printf("+%d", num + 2 * i);
//    }
//    printf("\n");
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int n = 0;
//    int m = 0;
//    while (scanf("%d", &n) != EOF) {
//        int count1 = 0;
//        int count2 = 0;
//        int sum = 0;
//        for (int i = 0; i < n; i++) {
//            scanf("%d", &m);
//            if (m > 0) {
//                sum += m;
//                count1++;
//            }
//            if (m < 0) {
//                count2++;
//            }
//        }
//        printf("%d ", count2);
//        if (count1)
//            printf("%.1f", sum * 1.0 / count1);
//        else
//            printf("0.0");
//    }
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int n = 0;
//    int arr[1000];
//    while (scanf("%d", &n) != EOF) {
//        int count1 = 0;
//        int count2 = 0;
//        int sum = 0;
//        for (int i = 0; i < n; i++) {
//            scanf("%d", &arr[i]);
//            if (arr[i] > 0) {
//                count1++;
//                sum += arr[i];
//            }
//            if (arr[i] < 0) {
//                count2++;
//            }
//        }
//        printf("%d ", count2);
//        if (count1)
//            printf("%.1f", sum * 1.0 / count1);
//        else
//            printf("0.0");
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	printf("abc\vdef");
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "abcdef";
//	int len = strlen(arr);
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d %u", len, sz);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abc\0def";
//	printf("%d\n", strlen(arr1));
//	printf("%d\n", strlen(arr2));
//	printf("%zd\n", sizeof(arr1) / sizeof(arr1[0]));
//	printf("%zd\n", sizeof(arr2) / sizeof(arr2[0]));
//	return 0;
//}
// 强制类型转换
//#include<stdio.h>
//int main()
//{
//	int a = (int)3.14;
//	printf("%d", a);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    float score = 0.0f;
//    float max = 0.0f;
//    float min = 100.0f;
//    float sum = 0.0f;
//    float ave = 0.0f;
//    for (int i = 1; i <= n; i++)
//    {
//        scanf("%f", &score);
//        sum += score;
//        ave = sum / n;
//        if (score > max)
//        {
//            max = score;
//        }
//        if (score < min)
//        {
//            min = score;
//        }
//    }
//    printf("%.2f %.2f %.2f", max, min, ave);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    double f = 0.0;
//    scanf("%lf", &f);
//    double c = 0.0;
//    c = 5 *1.0/ 9 * (f - 32);
//    printf("%.3lf", c);
//    return 0;
//}
#include <stdio.h>
#include<math.h>
int main()
{
    int x1 = 0;
    int x2 = 0;
    int y1 = 0;
    int y2 = 0;
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    int m = pow((x1 - x2), 2) + pow((y1 - y2), 2);
    printf("%d", m);
    return 0;
}