#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int count = 0;
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((n >> 1) & 1) == 1)
//		{
//			count++;
//		}
//	}
//	printf("%d", count);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int count = 0;
//	while (n)
//	{
//		count++;
//		n = n & (n - 1);
//	}
//	printf("%d", count);
//	return 0;
//}


// 二进制的置0或置1
//将第5位改为1
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	n = n | (1 << 4);
//	printf("%d\n", n);
//	n = n & ~(1 << 4);
//	printf("%d\n", n);
//	return 0;
//}



// Python测试
//name1 = 'abc'
//name2 = 'ABC'
//name3 = 'Abc'
//name4 = 'AbC'
//print(name1.upper())  # 大写
//print(name2.lower())  # 小写
//print(name3.upper())  # 大写
//print(name4.lower())  # 小写
//
//#  使首字母大写
//name5 = 'ada bood josk'
//print(name5.title())
//
//first_name = "ada"
//last_name = "love sam"
//full_name = f"{first_name} {last_name}"
//print(f"hello ,{full_name.title()}")
//

//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int m = 2 << (n - 1);
//    printf("%d", m);
//    return 0;
//}


//#include <stdio.h>
//
//int main() {
//    int age = 0;
//    scanf("%d", &age);
//    int time = 3156;
//    time = age * time;
//    printf("%d0000", time);
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//    int a, b, c, d;
//    scanf("%d %d %d %d", &a, &b, &c, &d);
//    int n = (a + b - c) * d;
//    printf("%d", n);
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//    float a, b, c, d;
//    scanf("%f %f %f %f", &a, &b, &c, &d);
//    float m = a * 0.2 + b * 0.1 + c * 0.2 + d * 0.5;
//    printf("%.1f", m);
//    return 0;
//}


#include <stdio.h>
#include<math.h>
int main() {
    int n = 0;
    scanf("%d", &n);
    int i = 0;
    int ret = 0;
    while (n) {
        int m = n % 10;
        if (0 == m % 2) {
            m = 0;
        }
        else {
            m = 1;
        }
        ret += m * pow(10, i++);
        n /= 10;
    }
    printf("%d\n", ret);
    return 0;
}