#define _CRT_SECURE_NO_WARNINGS


// 枚举类型  enum
//#include<stdio.h>
//enum Color
//{
//  // 枚举常量
//	RED,      // 0  无初始值的时候默认为 0
//	GREEN,    // 1
//	BLUE      // 2
//};
//int main()
//{
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", BLUE);
//	return 0;
//}
 

//#include<stdio.h>
//enum Color
//{
//	RED = 4,    // 4   可以赋初始值
//	GREEN,      // 5
//	BLUE        // 6
//};
//int main()
//{
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", BLUE);
//	return 0;
//}


// 使用
//#include<stdio.h>
//enum animal{
//	Dog, 
//	Cat,
//	Monkey, 
//	Invalid
//};
//void dog(void)
//{
//	puts("汪汪!!");
//}
//void cat(void)
//{
//	puts("喵喵!!");
//}
//void monkey(void)
//{
//	puts("唧唧!!");
//}
//enum animal select(void)
//{
//	int tmp;
//	do
//	{
//		printf("0...狗 1...猫 2...猴 3...结束------>");
//		scanf("%d", &tmp);
//	} while (tmp<Dog || tmp>Invalid);
//	return tmp;
//}
//int main()
//{
//	enum animal selectde;
//	do
//	{
//		switch(selectde =select())
//		{
//		case Dog:dog(); break;
//		case Cat:cat(); break;
//		case Monkey:monkey(); break;
//		}
//	} while (selectde != Invalid);
//	return 0;
//}
//


//#include<stdio.h>
//enum ENUM_A
//{
//	X1,
//	Y1,
//	Z1 = 255,
//	A1,
//	B1,
//};
//int main()
//{
//	enum ENUM_A enumA = Y1;
//	enum ENUM_A enumB = B1;
//	printf("%d %d\n", enumA, enumB);
//}


// 动态内存管理
// malloc
// free
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	// 开辟空间
//	int *p = (int *)malloc(20);
//	if (p == NULL)
//	{
//		perror(malloc);
//		return 1;
//	}
//	// 使用空间
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*(p + i) = i + 1;
//	}
//	// 释放空间
//	free(p);
//	p = NULL;
//	return 0;
//}


// calloc
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int* p = (int*)calloc(5,sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}


// realloc

//#include<stdio.h>
//int main()
//{
//	int* p = (int*)calloc(5, sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	// 使用
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*(p + i) = i + 1;
//	}
//	// 将空间调整为40个字节
//	int *ptr = (int *)realloc(p, 40);
//	if (ptr != NULL)
//	{
//		p = ptr;
//		for (i = 5; i < 10; i++)
//		{
//			*(p + i) = i + 1;
//		}
//		for (i = 5; i < 10; i++)
//		{
//			printf("%d ", *(p + i));
//		}
//		free(p);
//		p=NULL;
//	}
//	else
//	{
//		perror("relloc");
//		free(p);
//		p = NULL;
//	}
//	return 0;
//}


//#include<stdio.h>
//int check()
//{
//	union Un
//	{
//		char c;
//		int i;
//	}u;
//	u.i = 1;
//	return u.c;
//}
//int main()
//{
//	int ret = check();
//	if (ret == 1)
//		printf("小端\n");
//	else
//		printf("大端\n");
//	return 0;
//}


//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int ret = atoi("123");
//	printf("%d\n", ret);
//	return 0;
//}



//atio 的模拟实现
//#include<stdio.h>
//#include<assert.h>
//#include<ctype.h>
//#include <climits>
//int my_atio(const char* str)
//{
//	assert(str);
//	if (*str == '\0')
//	{
//		return 0;
//	}
//	while (isspace(*str))
//	{
//		str++;
//	}
//	int flag = 1;
//	if (*str == '+')
//	{
//		flag = 1;
//		str++;
//	}
//	else if(*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//	long long ret = 0;
//	while (*str != '\0')
//	{
//		if (isdigit(*str))
//		{
//			ret = ret *10 + (*str - '0')*flag;
//			if (ret > INT_MAX)
//			{
//				ret = INT_MAX;
//			}
//			if(ret < INT_MIN)
//			{
//				ret = INT_MIN;
//			}
//		}
//		else
//		{
//			return (int)ret;
//		}
//		str++;
//	}
//	return (int)ret;
//}
//int main()
//{
//	int ret = my_atio("123");
//	printf("%d\n", ret);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[50] = { 0 };
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int del = 0;
//	scanf("%d", &del);
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		if (arr[i] != del)
//		{
//			arr[j] = arr[i];
//			j++;
//		}
//	}
//	for (i = 0; i < j; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

#include<stdio.h>
#include<stdlib.h>
int main()
{
	int** arr = (int**)malloc(sizeof(int*) * 3);
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		arr[i] = (int*)malloc(sizeof(int) * 5);
	}
	int j = 0;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 5; j++)
		{
			arr[i][j] = 1;
		}
	}
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 5; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	for (i = 0; i < 3; i++)
	{
		free(arr[i]);
		arr[i] = NULL;
	}
	free(arr);
	arr = NULL;
	return 0;
}