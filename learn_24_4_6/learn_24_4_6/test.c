#define _CRT_SECURE_NO_WARNINGS

#include"SeqList.h"

void SLTest01()
{
	SL sl;
	SLInit(&sl);
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPrint(sl);

	//SLPushFront(&sl, 5);
	//SLPushFront(&sl, 6);

	// 测试尾删
	SLPopBack(&sl);
	SLPrint(sl);
	SLPopBack(&sl);
	SLPrint(sl);
	// 测试头删
	//SLPopFront(&sl);
	//SLPrint(sl);
	//SLPopFront(&sl);
	//SLPrint(sl);

	SLDestroy(&sl);
}


void SLTest02()
{
	SL sl;
	SLInit(&sl);
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPrint(sl);
	// 测试指定位置之前插入数据
	//SLInsert(&sl, 0, 99);
	//SLPrint(sl);


	// 测试删除指定位置的数据
	//SLErase(&sl, 0);
	//SLPrint(sl);

	// 测试顺序表的查找
	int find = SLFind(&sl, 4);
	if (find >= 0)
	{
		printf("Yes---> %d\n", find);
	}
	else
	{
		printf("No\n");
	}

	SLDestroy(&sl);
}

int main()
{
	//SLTest01();
	SLTest02();
	return 0;
}