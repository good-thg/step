﻿#pragma once


#include<stdio.h>
#include<stdlib.h>
#include<assert.h>


// 定义顺序表结构
//#define N 100
//
//// 静态顺序表
//
//struct SeqList
//{
//	int arr[N];
//	int size; //  有效数据个数
//};


// 动态顺序表
typedef int SLDataType;
typedef struct SeqList
{
	int *arr;
	int size; //  有效数据个数
	int capacity;// 空间大小
}SL;


// 初始化
void SLInit(SL* ps);

// 销毁
void SLDestroy(SL* ps);

// 打印
void SLPrint(SL s);


// 头部插⼊删除 / 尾部插⼊删除
void SLPushBack(SL* ps, SLDataType x);
void SLPushFront(SL* ps, SLDataType x);

void SLPopBack(SL* ps);
void SLPopFront(SL* ps);


//指定位置之前插⼊/删除数据
void SLInsert(SL* ps, int pos, SLDataType x);
void SLErase(SL* ps, int pos);
int SLFind(SL* ps, SLDataType x);