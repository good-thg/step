#define _CRT_SECURE_NO_WARNINGS

// memmove
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr + 2, arr, 20);  // 5 * sizeof(int)
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);   // 1 2 1 2 3 4 5 8 9 10
//	}
//	return 0;
//}


//#include<stdio.h>
//#include<assert.h>
//void *my_memmove(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	if (dest < src)
//	{
//		// 前 -> 后
//		while(num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	// 上面部分代码同 memcpy 的模拟实现
//	else
//	{
//		// 后 -> 前
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr + 2, arr, 20);  // 5 * sizeof(int)
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


// memset
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "hello world";
//	memset(arr, '*', 5);
//	printf("%s\n", arr);
//	return 0;
//}


// memcmp   内存比较   
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7 };
//	int arr2[] = { 1,2,3,4,8,8,8 };
//	int ret = memcmp(arr1, arr2, 6 * sizeof(int));   // -1
//	printf("%d\n", ret);
//	return 0;
//}