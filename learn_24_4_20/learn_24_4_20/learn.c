#define _CRT_SECURE_NO_WARNINGS
// 复习学过的知识

// strlen 计算字符串长度/大小
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "abcd";
//	size_t len = strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}

// 模拟实现 strlen
// 方法一
//#include<stdio.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	int count = 0;
//	assert(str);
//	while(*str != '\0')
//	{
//		str++;
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcd";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}
// 方法二
//#include<stdio.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	if (*str == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(str + 1);
//}
//int main()
//{
//	char arr[] = "abcd";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}
// 方法三
//#include<stdio.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	char* p = str;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p - str;
//}
//int main()
//{
//	char arr[] = "abcd";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}






// strcpy   字符串拷贝
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "hello world!";
//	char arr2[20] = { 0 };
//	strcpy(arr2, arr1);  // 把 arr1 中的字符串拷贝到 arr2 中
//	printf("%s\n", arr2);
//	return 0;
//}

// 模拟实现 strcpy
//#include<stdio.h>
//#include<assert.h>
//char* my_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[] = "hello world!";    // src
//	char arr2[20] = { 0 };           // dest
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}



// strcat 连接/追加字符串
#include<stdio.h>
int main()
{
	char arr1[20] = "abcd";
	char arr2[] = "efgh";
	strcat(arr1, arr2);
	printf("%s\n", arr1);   //  abcdefgh
	return 0;
}


// strcat 模拟实现
#include<stdio.h>
#include<assert.h>
char* my_strcat(char* dest, const char* src)
{
	assert(dest && src);
	char* ret = dest;
	while (*dest)
	{
		dest++;
	}
	while (*dest++ = *src++)
	{
		;
	}
	return ret;
}
int main()
{
	char arr1[20] = "abcd";   // dest
	char arr2[] = "efgh";     // src
	my_strcat(arr1, arr2);
	printf("%s\n", arr1);
	return 0;
}