#define _CRT_SECURE_NO_WARNINGS

// 内存操作函数
// 不负责内存重叠的
// memcpy
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[100] = { 0 };
//	memcpy(arr2, arr1, 40);  //  40 个字节
//	// 把 arr1 中的复制到arr2中，
//	for (int i = 0; i < sizeof(arr1) / sizeof(arr1[0]); i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[100] = { 0 };
//	memcpy(arr2, arr1, 20);  //  20 个字节
//	// 把 arr1 中的复制到arr2中，
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


// 模拟实现
//#include<stdio.h>
//#include<assert.h>
//void* my_memcpy(void* dest, const void* src, size_t count)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (count--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{ 
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };     // src
//	int arr2[100] = { 0 };                     // dest
//	my_memcpy(arr2, arr1, 5 * sizeof(int));
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//#include<stdio.h>
//#include<assert.h>
//void* my_memcpy(void* dest, const void* src, size_t count)
//{
//	assert(dest && src);
//	void* ret = dest;
//	for (int i = 0; i < count; i++)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };     // src
//	int arr2[100] = { 0 };                     // dest
//	my_memcpy(arr2, arr1, 5 * sizeof(int));
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}