#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main()
{
	int a, b, c;
	scanf("%d%d%d", &a, &b, &c);
	int t = 0;
	if (a < b)
	{
		t = a;
		a = b;
		b = t;
	}
	if (a < c)
	{
		t = a;
		a = c;
		c = t;
	}
	if (b < c)
	{
		t = b;
		b = c;
		c = t;
	}
	printf("%d %d %d", a, b, c);
	return 0;
}
#include<stdio.h>
int main()
{
	int i = 1;
	int count = 0;
	for (i = 1; i <= 100; i++)
	{
		if (i % 10 == 9)
			count++;
		if (i / 10 == 9)
			count++;
	}
	printf("1--100之间9的个数为：%d个", count);
	return 0;
}
#include <stdio.h>
int main()
{
    int a, b, c;
    while ((scanf("%d%d%d", &a, &b, &c)) != EOF)
    {
        if (a + b > c && a + c > b && b + c > a)
        {
            if (a == b && b == c)
                printf("Equilateral triangle!\n");
            else if (a == b && a != c || a == c && a != b || b == c && b != a)
                printf("Isosceles triangle!\n");
            else
                printf("Ordinary triangle!\n");
        }
        else
        {
            printf("Not a triangle!\n");
        }
    }
    return 0;
}
#include <stdio.h>
int main()
{
	int n = 0;
	while ((scanf("%d", &n)) != EOF)
	{
		int i = 0;
		for (i = 1; i <= n; i++)
		{
			printf("*");
		}
		printf("\n");
	}
	return 0;
}
#include <stdio.h>

int main()
{
	int a = 0;
	int b = 0;
	while ((scanf("%d %d", &a, &b)) != EOF)
	{

		if (a > b)
			printf("%d>%d\n", a, b);
		else if (a < b)
			printf("%d<%d\n", a, b);
		else
			printf("%d=%d\n", a, b);
	}
	return 0;
}
#include <stdio.h>

int main()
{
	int n = 0;
	scanf("%d", &n);
	if (n >= 140)
		printf("Genius\n");
	return 0;
}