//#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int r = a > b ? a : b;
//	printf("%d", r);
//	return 0;
//}
//# 集合
//# 集合的定义：
//# 大括号包围元素，每个元素直接用逗号隔开
//# 与字典的区别：字典键值对是一一对应的，集合是没有的
//# 举例：
//t1 = { 1,2,3,4,5,6,7,8,9 }
//print(t1)
//print(type(t1))
//# 用 set() 定义
//t2 = set("abv")
//print(t2)
//
//# 定义一个空集合
//t3 = set()
//print(type(t3))
//
//# 集合的特性：无序不重复的数据类型，不支持下标操作
//
//# 集合的操作
//# 增
//# 1.add()   添加某个具体元素
//s1 = { 10,20,30 }
//s1.add(12)
//print(s1)
//# 2.update()  追加的数据必须是可迭代对象(列表、集合、元组……）
//	s2 = { 10,20 }
//	s2.update([100])
//	print(s2)
//
//	# 删除
//	# 1.remove()  删除指定的数据，数据不存在则报错
//	s1 = { 10,20 }
//	s1.remove(10)
//	print(s1)
//	# 2.discard()  删除指定数据，数据不存在不报错
//	s2 = { 10,20 }
//	s2.discard(20)
//	print(s2)
//	# 3.pop()   随机删除集合中的一个数据，并返回这个数据
//	s3 = { 1,2,3,5,9 }
//	s4 = s3.pop()
//	print(s4)
//	print(s3)
//
//
//	# 交集 & 同数学
//	s1 = { 1,2,3,4 }
//	s2 = { 3,4,5,6 }
//	s3 = s1 & s2
//	print(s3)
//	# 并集 | 同数学
//	s4 = { 1,2,3,4 }
//	s5 = { 3,4,5,6 }
//	s6 = s1 | s2
//	print(s6)
//
//
//	# 公共操作
//	# 1. + 合并        字符串，列表，元组
//	# 2. * 复制        字符串，列表，元组
//	# 3. in     元素存在     字符串，列表，元组，集合
//	# 4.not in  元素不存在   字符串，列表，元组，结合
//	# 公共方法
//	# 1.len()   字符串，列表，元组，集合，字典
//	# 集合举例
//	s1 = { 10,20,30 }
//	print(len(s1))
//	# 2.max()
//	list1 = [10, 20, 56, 98]
//	print(max(list1))
//	# 3.min()
//	print(min(list1))
//	# 4.enumerate()  效果（下标，数据）一一列出，通常用for遍历
//	list2 = ['a', 'b', 'c']
//	for i in enumerate(list2) :
//		print(i)
//		# 5.del()    字符串，列表
//		# str1 = "adc"
//		# del str1    # 没有指定删除某个元素，而是删除整个对象，表示删除所有数据
//		# print(str1)
//		# 6.range()
//		for i in range(5) :
//			print(i)
//
//
//			# 容器类型推导式
//			# 列表推导式
//			# while
//			list1 = []
//			i = 0
//			while i < 10:
//list1.append(i)
//i += 1
//print(list1)
//
//# for
//list2 = []
//for i in range(10) :
//	list2.append(i)
//	print(list2)
//
//	# 列表推导式
//	list3 = [i for i in range(10)]
//	print(list3)
//
//	# 元组推导式
//	t1 = (i for i in range(10))
//	print(t1)        # 返回的对象地址
//	print(tuple(t1))
//
//	# 字典推导式
//	dict1 = { i:i * *2 for i in range(1,5) }
//	print(dict1)
//
//	# 集合的推导式
//	list1 = [1, 1, 2]
//	set1 = { i * *2 for i in list1 }
//	print(set1)  # 去重