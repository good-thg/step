#define _CRT_SECURE_NO_WARNINGS

// strcmp  字符串比较
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "aba";
//	char arr2[] = "abc";
//	int ret = strcmp(arr1, arr2);  // 在之前的关机程序中使用过  字符串相等返回0
//	printf("%d\n", ret);
//	return 0;
//}


// strcmp的模拟实现
//#include<stdio.h>
//#include<assert.h>
//int my_strcmp(const char* str1, const  char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2; 
//}
//int main()
//{
//	char arr1[] = "abcd";
//	char arr2[] = "abcd";
//	int ret = my_strcmp(arr1, arr2);    //  arr1 > arr2   > 0  arr1 < arr2   < 0  arr1 == arr2  0
//	printf("%d\n", ret);
//	return 0;
//}


//#include<stdio.h>
//#include<assert.h>
//int my_strcmp(const char* str1, const  char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//	{
//		return 1;
//	}
//	else
//	{
//		return -1;
//	}
//}
//int main()
//{
//	char arr1[] = "abca";
//	char arr2[] = "abcd";
//	int ret = my_strcmp(arr1, arr2);    //  arr1 > arr2   > 0  arr1 < arr2   < 0  arr1 == arr2  0
//	printf("%d\n", ret);
//	return 0;
//}



// strncpy     拷贝 n 个字符
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "abcdefgh";
//	char arr2[20] = { 0 };
//	strncpy(arr2, arr1, 2); // 拷贝2个字符到arr2中
//	printf("%s\n", arr2);
//	return 0;
//}


// strncat    追加 n 个字符
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[20] = "abcd";
//	char arr2[] = "efgh";
//	strncat(arr1, arr2, 2);  // 在arr1后面追加2个字符
//	printf("%s\n", arr1);
//	return 0;
//}


// strncmp   比较 n 个字符
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "abcdefgh";
//	char arr2[] = "abcdef";
//	int ret = strncmp(arr1, arr2, 4);   // 比较4个字符
//	printf("%d\n", ret);
//	return 0;
//}


// strstr  在一个字符串中查找另一个字符串，返回的是出现的第一次的位置
// 没有出现返回空指针
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "That is an apple";
//	const char* p = "is";
//	char *ret = strstr(arr1, p);
//	printf("%s\n", ret);
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "That is an apple";
//	char arr2[] = "is";
//	char* ret = strstr(arr1, arr2);
//	printf("%s\n", ret);
//	return 0;
//}
 


// 模拟实现 strstr
//#include<stdio.h>
//char* my_strstr(const char* str1, const char* str2)
//{
//	const char *s1 = NULL;
//	const char *s2 = NULL;
//	const char* cur = str1;
//	// str2 为空时返回 str1
//	if (*str2 == '\0')
//	{
//		return (char *)str1;
//	}
//	while (*cur)
//	{
//		s1 = cur;
//		s2 = str2;
//		while (*s1 != '\0' && *s2 != '\0' && * s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return (char *)cur;
//		}
//		cur++;
//	}
//	// 找不到返回 NULL(空指针)
//	return NULL;
//}
//int main()
//{
//	char arr1[] = "This is an apple";
//	const char* p = "is";
//	char* ret = my_strstr(arr1, p);
//	printf("%s\n", ret);
//	return 0;
//}


// strtok     字符串切分
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "192.hello@3325";
//	const char* sep = ".@";
//	char* str = NULL;
//	str = strtok(arr, sep);
//	printf("%s\n", str);
//	str = strtok(NULL, sep);
//	printf("%s\n", str);
//	str = strtok(NULL, sep);
//	printf("%s\n", str);
//	return 0;
//}
////192
  //hello
  //3325



//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "192.hello@3325";
//	const char* sep = ".@";
//	char* str = NULL;
//	for (str = strtok(arr, sep); str != NULL; str = strtok(NULL, sep)) 
//	{
//		printf("%s\n", str);
//	}
//	//192
//	//hello
//	//3325
//	return 0;
//}


// strerror    打印错误码
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%s\n", strerror(i));
//	}
//	return 0;
//}



//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	// 打开文件
//	FILE* pf = fopen("learn.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	// 读文件
//	
//	// 关闭文件
//	fclose(pf);
//}


// perror  有能力直接打印错误信息,相当于对 strerror 的封装
//#include<stdio.h>
//int main()
//{
//	// 打开文件
//	FILE* pf = fopen("learn.txt", "r");
//	if (pf == NULL)
//	{
//		perror("problem");   // problem: No such file or directory
//		return 1;
//	}
//	// 读文件
//
//	// 关闭文件
//	fclose(pf);
//}


