#define _CRT_SECURE_NO_WARNINGS
//int removeElement(int* nums, int numsSize, int val) {
//    /*
//    当src == val 的时候,src++;
//    当src != val 的时候，src++,dest++
//    最后的dest就是有效长度
//    */
//    // 创建两个变量
//    int src = 0;
//    int dest = 0;
//    while (src < numsSize) {
//        if (nums[src] == val) {
//            src++;
//        }
//        else {
//            nums[dest] = nums[src];
//            dest++;
//            src++;
//        }
//    }
//    return dest;
//}

//void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
//    int l1 = m - 1;
//    int l2 = n - 1;
//    int l3 = m + n - 1;
//    // 只要有一个条件为假，就跳出循环
//    while (l1 >= 0 && l2 >= 0)
//    {
//        if (nums1[l1] < nums2[l2])
//        {
//            nums1[l3] = nums2[l2];
//            l3--;
//            l2--;
//        }
//        else
//        {
//            nums1[l3] = nums1[l1];
//            l3--;
//            l1--;
//        }
//    }
//    // 出循环 l1 < 0 or l2 < 0 但是只需要处理 l1 < 0 的情况
//    // 因为 l1 < 0 的时候说明 nums2 中还有数据没有存储到 nums1中
//    while (l2 >= 0)
//    {
//        nums1[l3] = nums2[l2];
//        l3--;
//        l2--;
//    }
//
//}


// 上三角矩阵的判断
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	int j = 0;
//	int arr[10][10] = { 0 };
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < n; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < n; j++)
//		{
//			if (i > j)
//			{
//				if (arr[i][j] != 0)
//				{
//					printf("NO\n");
//					return 0;
//				}
//			}
//		}
//	}
//	printf("Yes\n");
//	return 0;
//}



//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	int j = 0;
//	int arr[10][10] = { 0 };
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < n; j++)
//		{
//			scanf("%d", &arr[i][j]);
//			if (i > j)
//			{
//				if (arr[i][j] != 0)
//				{
//					printf("NO\n");
//					return 0;
//				}
//			}
//		}
//	}
//	printf("Yes\n");
//	return 0;
//}



// 矩阵转置
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	int i = 0;
//	int j = 0;
//	int arr[10][10] = { 0 };
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	for (i = 0; i < m; i++)
//	{
//		for (j = 0; j < n; j++)
//		{
//			printf("%d ", arr[j][i]);
//		}
//		printf("\n");
//	}
//	return 0;
//}



// 矩阵交换
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	int i = 0;
//	int j = 0;
//	int arr[10][10] = { 0 };
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	// 操作次数
//	int k = 0;
//	scanf("%d", &k);
//	// r 表示操作行 c 表示操作列
//	char t = 0;
//	int a = 0;
//	int b = 0;
//	for (i = 0; i < k; i++)
//	{
//		scanf(" %c %d %d", &t, &a, &b); // 加空格过滤所有空白字符
//		if (t == 'r')  // 交换行
//		{
//			for (j = 0; j < m; j++)
//			{
//				int tmp = arr[a - 1][j];
//				arr[a - 1][j] = arr[b - 1][j];
//				arr[b - 1][j] = tmp;
//			}
//		}
//		else if (t == 'c')  // 交换列
//		{
//			for (j = 0; j < n; j++)
//			{
//				int tmp = arr[j][a - 1];
//				arr[j][a - 1] = arr[j][b - 1];
//				arr[j][b - 1] = tmp;
//			}
//		}
//	}
//	// 打印
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


// 杨辉三角
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	int j = 0;
//	int arr[30][30] = { 0 };
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < n; j++)
//		{
//			if (j == 0 || i == j)
//			{
//				arr[i][j] = 1;
//			}
//			if (i >= 2 && j >= 1)
//			{
//				arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
//			}
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}



/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
//typedef struct ListNode ListNode;
//struct ListNode* middleNode(struct ListNode* head) {
//    // 快慢指针
//    ListNode* slow = head;
//    ListNode* fast = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//    }
//    return slow;
//}


