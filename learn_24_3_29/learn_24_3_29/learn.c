#define _CRT_SECURE_NO_WARNINGS

// 匿名结构体
// 只能使用一次
//#include<stdio.h>
//struct
//{
//	char a;
//	int b;
//	float c;
//}m = {'m',12,3.14};
//int main()
//{
//	printf("%c %d %f", m.a, m.b, m.c);
//	return 0;
//}


//#include<stdio.h>
//typedef struct
//{
//	char a;
//	int b;
//	float c;
//}m;
//int main()
//{
//	m n;
//	return 0;
//}

// 不能是匿名的
//#include<stdio.h>
//struct Node
//{
//	int date;
//	struct Node *next;
//};
//int main()
//{
//	struct Node n1;
//	return 0;
//}


//#include<stdio.h>
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct S1));
//	return 0;
//}

//#include<stdio.h>
//struct S2
//{
//	char c1;
//	char c2;
//	int i;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct S2));
//	return 0;
//}

//#include<stdio.h>
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct S3));
//	return 0;
//}


//#include<stdio.h>
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct S4));
//	return 0;
//}



//#include <stdio.h>
//#pragma pack(1)//设置默认对?数为1
//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()//取消设置的对?数，还原为默认
//int main()
//{
//	printf("%d\n", sizeof(struct S));
//	return 0;
//}


//#include<stdio.h>
//struct S
//{
//	int arr[1000];
//	int n;
//	double d;
//};
//void print1(struct S tmp)
//{
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", tmp.arr[i]);
//	}
//	printf("%d ", tmp.n);
//	printf("%.lf", tmp.d);
//}
//int main()
//{
//	struct S s = { {1,2,3,4,5},5,3.14 };
//	print1(s);
//	return 0;
//}



//#include<stdio.h>
//struct S
//{
//	int arr[1000];
//	int n;
//	double d;
//};
//void print1(struct S *p)
//{
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", p->arr[i]);
//	}
//	printf("%d ", p->n);
//	printf("%.lf", p->d);
//}
//int main()
//{
//	struct S s = { {1,2,3,4,5},100,3.14 };
//	print1(&s);
//	return 0;
//}


//#include<stdio.h>
//union Un
//{
//	char c;
//	int i;
//};
//int main()
//{
//	union Un u = { 0 };
//	printf("%zd\n", sizeof(u));
//	printf("%p\n", &u);
//	printf("%p\n", &(u.c));
//	printf("%p\n", &(u.i));
//	return 0;
//}
//
//


//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[40] = { 0 };
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	for (i = 0; i < n - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < n - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//	for (i = n - 1; i >= n - 5; i--)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}