#define _CRT_SECURE_NO_WARNINGS
//指针
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	//变量创建的本质是在内存中开辟一块空间
//	printf("%p", &a);
//	return 0;
//}
// 
// 
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;//取出a的地址存放在指针变量p中
//	printf("%p", p);
//	return 0;
//}
// 
// 
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	*p = 100;
//	printf("%d", a);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	printf("%zd\n", sizeof(char*));
//	printf("%zd\n", sizeof(short*));
//	printf("%zd\n", sizeof(int*));
//	printf("%zd\n", sizeof(double*));
//	return 0;
//}


//#include<stdio.h>
//void reverse(char* left, char* right)
//{
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char arr[101] = { 0 };
//	gets(arr);
//	int len = strlen(arr);
//	reverse(arr, arr + len - 1);
//	char* start = arr;
//	while (*start)
//	{
//		char* end = start;
//		while (*end != ' ' && *end != '\0')
//		{
//			end++;
//		}
//		reverse(start, end - 1);
//		if (*end != '\0')
//			end++;
//		start = end;
//	}
//	printf("%s", arr);
//	return 0;
//}