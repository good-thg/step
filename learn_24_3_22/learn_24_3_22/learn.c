﻿#define _CRT_SECURE_NO_WARNINGS

//#include<stdio.h>
//char* my_strstr(const char* str1, const char* str2)
//{
//	const char* s1 = NULL;
//	const char* s2 = NULL;
//	const char* cur = str1;
//	if (*str2 == '\0')
//		return (char*)str1;
//	while (*cur)
//	{
//		s1 = cur;
//		s2 = str2;
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return (char*)cur;
//		}
//		cur++;
//	}
//	return NULL;
//}
//int main()
//{
//	char arr1[] = "abcdefgh";
//	char arr2[] = "cde";
//	char* ret = my_strstr(arr1, arr2);
//	if (ret == NULL)
//	{
//		printf("NO\n");
//	}
//	else
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}



//#include<stdio.h>
//int main()
//{
//	int a[5] = { 5, 4, 3, 2, 1 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int aa[2][5] = { 10,9,8,7,6,5,4,3,2,1 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}


// strtok的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "abc.hhi@kl.hahaha@aaa.12345";
//	char arr2[30] = { 0 };
//	strcpy(arr2, arr1);
//	const char* sep = "@.";
//	char *ret = NULL;
//
//	for (ret = strtok(arr1, sep); ret != NULL; ret = strtok(NULL, sep))
//	{
//		printf("%s\n",ret);
//	}
//	return 0;
//}

// strerror的使用
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 10; i++)
//	{
//		printf("%d--->%s\n",i, strerror(i));
//	}
//	return 0;
//}



//#include <stdio.h>
//#include <string.h>
//#include <errno.h>
//int main()
//{
//	FILE* pFile;
//	pFile = fopen("learn.txt", "r");
//	if (pFile == NULL)
//		printf("%s\n", strerror(errno));
//	fclose(pFile);
//	return 0;
//}



// perror可以直接打印错误信息
//#include <stdio.h>
//#include <string.h>
//#include <errno.h>
//int main()
//{
//	FILE* pFile;
//	pFile = fopen("learn.txt", "r");
//	if (pFile == NULL)
//		perror("hahah");
//	return 0;
//}




// C语言内存函数
// memcpy-------->针对内存块拷贝
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	memcpy(arr2, arr1, 20);   // 前5个整型，20个字节
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}

// 模拟实现
//#include <stdio.h>
//#include<assert.h>
//void *my_memcpy(const void* dest, void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		src = (char*)src + 1;
//		dest = (char*)dest + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	my_memcpy(arr2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}
//


// 如果有重叠，则结果未定义的,但是在当前库中是可以重叠的
//#include <stdio.h>
//#include<assert.h>
//void* my_memcpy(const void* dest, void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		src = (char*)src + 1;
//		dest = (char*)dest + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memcpy(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}


// memmove 使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}

//模拟实现
//#include<stdio.h>
//#include<assert.h>
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	if (dest < src)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}




// memset 是⽤来设置内存的，将内存中的值以字节为单位设置成想要的内容

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char str[] = "hello world";
//	memset(str, 'x', 6);   // 这里是以字节为单位
//	printf(str);
//	return 0;
//}


// memcmp

//#include<stdio.h>
//#include<string.h>
//int main()
//{	
//	int arr1[] = { 1,2,3,4,5,6 };
//	int arr2[] = { 1,2,3,4,6,6 };
//	int ret = memcmp(arr1, arr2, 20);   //字节为单位
//	printf("%d\n", ret);
//	return 0;
//}



//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char buffer1[] = "DWgaOtP12df0";
//	char buffer2[] = "DWGAOTP12DF0";
//	int n;
//	n = memcmp(buffer1, buffer2, sizeof(buffer1));
//	if (n > 0)
//		printf("'%s' is greater than '%s'.\n", buffer1, buffer2);
//	else if (n < 0)
//		printf("'%s' is less than '%s'.\n", buffer1, buffer2);
//	else
//		printf("'%s' is the same as '%s'.\n", buffer1, buffer2);
//	return 0;
//}



// 数据在内存中的存储
//#include <stdio.h>
//int main()
//{
//	int a = 0x11223344;
//	return 0;
//}



// 设计程序判断字节序

//#include <stdio.h>
//int check_sys()
//{
//	int n = 1;
//	if (*(char*)&n == 1)
//		return 1;
//	else
//		return 0;
//}
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//	{
//		printf("⼩端\n");
//	}
//	else
//	{
//		printf("⼤端\n");
//	}
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char a = -1;
//	signed char b = -1;
//	unsigned char c = -1;
//	printf("a=%d,b=%d,c=%d", a, b, c);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char a = -128;
//	printf("%u\n", a);
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	char a = 128;
//	printf("%u\n", a);
//	return 0;
//}


// strncpy
//#include<stdio.h>
//char* my_strncpy(char *dest,const char *src,int n)
//{
//	int i = 0;
//	for (i = 0; src[i] != '\0' && i < n; i++)
//	{
//		dest[i] = src[i];
//	}
//	dest[i] = '\0';
//	return dest;
//}
//int main()
//{
//	char arr1[] = "abcd";
//	char arr2[20] = "abcdef";
//	int ret = my_strncpy(arr1, arr2, 4);
//	printf("%d\n", ret);
//	return 0;
//}


//#include<stdio.h>
//int check_sys()
//{
//	int a = 0x11223344;
//	char* p = (char*) & a;
//	if (*p == 0x44)
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}