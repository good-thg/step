#define _CRT_SECURE_NO_WARNINGS
// sizeof  单目操作符  只关注占用内存空间的大小，不在乎内存中存放什么数据
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	printf("%zd\n", sizeof(a));
//	printf("%zd\n", sizeof(int));
//
//	return 0;
//}
// 
// 
//  strlen  函数   求字符串长度
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int len = strlen("abcdfoj");
//	printf("%d\n", len);
//	return 0;
//}


//sizeof(数组名)：数组名表示整个数组，计算的是整个数组的大小，单位是字节
//&数组名：数组名表示的是整个数组，取出的是整个数组的地址
//除此以外，所有的数组名是数组首元素的地址
//
//#include<stdio.h>
//int main()
//{
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));
//	printf("%d\n", sizeof(a + 0));
//	printf("%d\n", sizeof(*a));
//	printf("%d\n", sizeof(a + 1));
//	printf("%d\n", sizeof(a[1]));
//	printf("%d\n", sizeof(&a));
//	printf("%d\n", sizeof(*&a));
//	printf("%d\n", sizeof(&a + 1));
//	printf("%d\n", sizeof(&a[0]));
//	printf("%d\n", sizeof(&a[0] + 1));
//	return 0;
//}

//#include<stdio.h>
//int max(int x, int y)
//{
//	int z;
//	if (x > y)
//		z = x;
//	else
//		z = y;
//	return z;
//}
//int main()
//{
//	int a, b, c;
//	scanf("%d %d", &a,&b);
//	c = max(a, b);
//	printf("max=%d", c);
//	return 0;
//}


