#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//#include <math.h>
//#include <windows.h>
//#include <tchar.h>
//
//float f(float x, float y, float z) {
//    float a = x * x + 9.0f / 4.0f * y * y + z * z - 1;
//    return a * a * a - x * x * z * z * z - 9.0f / 80.0f * y * y * z * z * z;
//}
//
//float h(float x, float z) {
//    for (float y = 1.0f; y >= 0.0f; y -= 0.001f)
//        if (f(x, y, z) <= 0.0f)
//            return y;
//    return 0.0f;
//}
//
//int main() {
//    system("color 0c");
//    HANDLE o = GetStdHandle(STD_OUTPUT_HANDLE);
//    _TCHAR buffer[25][80] = { _T(' ') };
//    _TCHAR ramp[] = _T(".:-=+*#%@");
//
//    for (float t = 0.0f;; t += 0.1f) {
//        int sy = 0;
//        float s = sinf(t);
//        float a = s * s * s * s * 0.2f;
//        for (float z = 1.3f; z > -1.2f; z -= 0.1f) {
//            _TCHAR* p = &buffer[sy++][0];
//            float tz = z * (1.2f - a);
//            for (float x = -1.5f; x < 1.5f; x += 0.05f) {
//                float tx = x * (1.2f + a);
//                float v = f(tx, 0.0f, tz);
//                if (v <= 0.0f) {
//                    float y0 = h(tx, tz);
//                    float ny = 0.01f;
//                    float nx = h(tx + ny, tz) - y0;
//                    float nz = h(tx, tz + ny) - y0;
//                    float nd = 1.0f / sqrtf(nx * nx + ny * ny + nz * nz);
//                    float d = (nx + ny - nz) * nd * 0.5f + 0.5f;
//                    *p++ = ramp[(int)(d * 5.0f)];
//                }
//                else
//                    *p++ = ' ';
//            }
//        }
//
//        for (sy = 0; sy < 25; sy++) {
//            COORD coord = { 0, sy };
//            SetConsoleCursorPosition(o, coord);
//            WriteConsole(o, buffer[sy], 79, NULL, 0);
//        }
//        Sleep(33);
//    }
//}

//#include <stdio.h>
//typedef struct {
//    char name[20];  
//    int id;         
//    float scores[3]; 
//    float average;  
//} Student;
//float calculateAverage(float* scores) {
//    float sum = 0;
//    for (int i = 0; i < 3; i++) {
//        sum += *(scores + i);  
//    }
//    return sum / 3.0;
//}
//int main() {
//    Student students[3] = {
//        {"张三", 1},
//        {"李四", 2},
//        {"王五", 3}
//    };
//    printf("请输入三个同学的语文、数学、英语成绩：\n");
//    for (int i = 0; i < 3; i++) {
//        printf("请输入%s的成绩：\n", students[i].name);
//        for (int j = 0; j < 3; j++) {
//            printf("第%d科成绩：", j + 1);
//            scanf("%f", &students[i].scores[j]);  
//        }
//        students[i].average = calculateAverage(students[i].scores);
//    }
//    printf("\n学生三科平均成绩如下：\n");
//    for (int i = 0; i < 3; i++) {
//        printf("%s的平均成绩是：%.2f\n", students[i].name, students[i].average);
//    }
//    return 0;
//}



//#include <stdio.h>
//#include <string.h>
//
//#define MAX_STUDENTS 100
//
//typedef struct {
//    char name[50];
//    int id;
//    float score;
//} Student;
//
//Student students[MAX_STUDENTS];
//int count = 0;
//
//void createStudents() {
//    int id;
//    printf("输入学生信息：\n");
//    printf("姓名：");
//    scanf("%s", students[count].name);
//    printf("学号：");
//    scanf("%d", &id);
//    printf("成绩：");
//    scanf("%f", &students[count].score);
//    students[count].id = id;
//    count++;
//}
//
//void displayStudents() {
//    printf("当前学生列表：\n");
//    for (int i = 0; i < count; i++) {
//        printf("姓名：%s，学号：%d，成绩：%.2f\n", students[i].name, students[i].id, students[i].score);
//    }
//}
//
//void searchByName(const char* name) {
//    printf("按姓名查询结果：%s：\n", name);
//    for (int i = 0; i < count; i++) {
//        if (strcmp(students[i].name, name) == 0) {
//            printf("姓名：%s，学号：%d，成绩：%.2f\n", students[i].name, students[i].id, students[i].score);
//        }
//    }
//}
//
//void searchByID(int id) {
//    printf("按学号查询结果：%d：\n", id);
//    for (int i = 0; i < count; i++) {
//        if (students[i].id == id) {
//            printf("姓名：%s，学号：%d，成绩：%.2f\n", students[i].name, students[i].id, students[i].score);
//            return;
//        }
//    }
//    printf("没有找到学号为%d的学生。\n", id);
//}
//
//void bubbleSort(char type) {
//    for (int i = 0; i < count - 1; i++) {
//        for (int j = 0; j < count - i - 1; j++) {
//            Student temp;
//            if (type == 'n') {
//                if (strcmp(students[j].name, students[j + 1].name) < 0) {
//                    temp = students[j];
//                    students[j] = students[j + 1];
//                    students[j + 1] = temp;
//                }
//            }
//            else if (type == 'i') {
//                if (students[j].id < students[j + 1].id) {
//                    temp = students[j];
//                    students[j] = students[j + 1];
//                    students[j + 1] = temp;
//                }
//            }
//            else if (type == 's') {
//                if (students[j].score < students[j + 1].score) {
//                    temp = students[j];
//                    students[j] = students[j + 1];
//                    students[j + 1] = temp;
//                }
//            }
//        }
//    }
//}
//
//int main() {
//    int choice;
//    printf("1. 添加学生\n");
//    printf("2. 显示学生列表\n");
//    printf("3. 按姓名查询\n");
//    printf("4. 按学号查询\n");
//    printf("5. 按姓名排序\n");
//    printf("6. 按学号排序\n");
//    printf("7. 按成绩排序\n");
//    printf("8. 退出\n");
//    printf("请输入您的选择：");
//    scanf("%d", &choice);
//
//    while (choice != 8) {
//        switch (choice) {
//        case 1:
//            if (count < MAX_STUDENTS) {
//                createStudents();
//            }
//            else {
//                printf("学生数量已达上限。\n");
//            }
//            break;
//        case 2:
//            displayStudents();
//            break;
//        case 3:
//            char name[50];
//            printf("请输入要查询的姓名：");
//            scanf("%s", name);
//            searchByName(name);
//            break;
//        case 4:
//            int id;
//            printf("请输入要查询的学号：");
//            scanf("%d", &id);
//            searchByID(id);
//            break;
//        case 5:
//            bubbleSort('n');
//            printf("已按姓名排序。\n");
//            break;
//        case 6:
//            bubbleSort('i');
//            printf("已按学号排序。\n");
//            break;
//        case 7:
//            bubbleSort('s');
//            printf("已按成绩排序。\n");
//            break;
//        }
//        printf("\n1. 添加学生\n");
//        printf("2. 显示学生列表\n");
//        printf("3. 按姓名查询\n");
//        printf("4. 按学号查询\n");
//        printf("5. 按姓名排序\n");
//        printf("6. 按学号排序\n");
//        printf("7. 按成绩排序\n");
//        printf("8. 退出\n");
//        printf("请输入您的选择：");
//        scanf("%d", &choice);
//    }
//    return 0;
//}


#include <stdio.h>
#include <string.h>

#define MAX_STUDENTS 100

typedef struct {
    char name[50];
    int id;
    float score;
} Student;

Student students[MAX_STUDENTS];
int count = 0;

void createStudents() {
    int id;
    printf("输入学生信息：\n");
    printf("姓名：");
    scanf("%s", students[count].name);
    printf("学号：");
    scanf("%d", &id);
    printf("成绩：");
    scanf("%f", &students[count].score);
    students[count].id = id;
    count++;
}

void displayStudents() {
    printf("当前学生列表：\n");
    for (int i = 0; i < count; i++) {
        printf("姓名：%s，学号：%d，成绩：%.2f\n", students[i].name, students[i].id, students[i].score);
    }
}

void searchByName(const char* name) {
    printf("按姓名查询结果：%s：\n", name);
    for (int i = 0; i < count; i++) {
        if (strcmp(students[i].name, name) == 0) {
            printf("姓名：%s，学号：%d，成绩：%.2f\n", students[i].name, students[i].id, students[i].score);
        }
    }
}

void searchByID(int id) {
    printf("按学号查询结果：%d：\n", id);
    for (int i = 0; i < count; i++) {
        if (students[i].id == id) {
            printf("姓名：%s，学号：%d，成绩：%.2f\n", students[i].name, students[i].id, students[i].score);
            return;
        }
    }
    printf("没有找到学号为%d的学生。\n", id);
}

void bubbleSort(char type) {
    for (int i = 0; i < count - 1; i++) {
        for (int j = 0; j < count - i - 1; j++) {
            Student temp;
            if (type == 'n') {
                if (strcmp(students[j].name, students[j + 1].name) < 0) {
                    temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
            else if (type == 'i') {
                if (students[j].id < students[j + 1].id) {
                    temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
            else if (type == 's') {
                if (students[j].score < students[j + 1].score) {
                    temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
        }
    }
}

int main() {
    int choice;
    printf("1. 添加学生\n");
    printf("2. 显示学生列表\n");
    printf("3. 按姓名查询\n");
    printf("4. 按学号查询\n");
    printf("5. 按姓名排序\n");
    printf("6. 按学号排序\n");
    printf("7. 按成绩排序\n");
    printf("8. 退出\n");
    printf("请输入您的选择：");
    scanf("%d", &choice);

    while (choice != 8) {
        switch (choice) {
        case 1:
            if (count < MAX_STUDENTS) {
                createStudents();
            }
            else {
                printf("学生数量已达上限。\n");
            }
            break;
        case 2:
            displayStudents();
            break;
        case 3:
            char name[50];
            printf("请输入要查询的姓名：");
            scanf("%s", name);
            searchByName(name);
            break;
        case 4:
            int id;
            printf("请输入要查询的学号：");
            scanf("%d", &id);
            searchByID(id);
            break;
        case 5:
            bubbleSort('n');
            displayStudents();
            break;
        case 6:
            bubbleSort('i');
            displayStudents();
            break;
        case 7:
            bubbleSort('s');
            displayStudents();
            break;
        }
        printf("\n1. 添加学生\n");
        printf("2. 显示学生列表\n");
        printf("3. 按姓名查询\n");
        printf("4. 按学号查询\n");
        printf("5. 按姓名排序\n");
        printf("6. 按学号排序\n");
        printf("7. 按成绩排序\n");
        printf("8. 退出\n");
        printf("请输入您的选择：");
        scanf("%d", &choice);
    }
    return 0;
}