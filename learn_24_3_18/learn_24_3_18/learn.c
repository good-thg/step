#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f' };
//	printf("%zd\n", sizeof(arr));
//	// 6    数组名单独放在sizeof内部，计算的是数组的大小，单位是字节
//	printf("%zd\n", sizeof(arr + 0));
//	// 4/8  arr是数组名，表示数组首元素的地址，arr + 0 还是首元素的地址，大小是4/8个字节
//	printf("%zd\n", sizeof(*arr));
//	// 1    arr是首元素的地址，*arr就是首元素的地址，大小是1个字节
//	printf("%zd\n", sizeof(arr[1]));
//	// 1    arr[1]是第二个元素的地址，大小是1个字节
//	printf("%zd\n", sizeof(&arr));
//	// 4/8  &arr是数组的地址，大小是4/8个字节
//	printf("%zd\n", sizeof(&arr + 1));
//	// 4/8  &arr + 1跳过了整个数组，指向了数组后边的空间，大小是4/8个字节
//	printf("%zd\n", sizeof(&arr[0] + 1));
//	// 4/8  &arr[0] + 1是第二个元素地址，是地址就是4/8个字节
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f' };
//	printf("%zd\n", strlen(arr));
//	// arr是首元素的地址，数组中没有\0，就会导致越界访问，结果是随机值
//	printf("%zd\n", strlen(arr + 0));
//	// arr + 0是数组首元素的地址，数组中没有\0，就会导致越界访问，结果是随机值
//	printf("%zd\n", strlen(*arr));
//	// arr是首元素的地址，*arr是首元素,就是'a','a'的ASCII码值是97，就相当于把97
//	// 作为地址传给了strlen,strlen得到的就是野指针
//	printf("%zd\n", strlen(arr[1]));
//	// arr[1]就是第二个元素'b'，'b'的ASCII码值是98，结果同上
//	printf("%zd\n", strlen(&arr));
//	// &arr是数组的地址，起始位置是数组第一个元素的位置，结果是随机值
//	printf("%zd\n", strlen(&arr + 1));
//	// 随机值
//	printf("%zd\n", strlen(&arr[0] + 1));
//	// 从第二个元素后开始统计的，随机值
//	return 0;
//}



//#include<stdio.h>
//int main()
//{
//	char arr[] = "abcdef";
//	printf("%zd\n", sizeof(arr));
//	// 7    arr是数组名，单独放在sizeof内部，计算的是数组总大小，是7个字节
//	printf("%zd\n", sizeof(arr + 0));
//	// 4/8  arr是数组首元素的地址，arr + 0还是数组首元素的地址，是4/8个字节
//	printf("%zd\n", sizeof(*arr));
//	// 1    arr是首元素的地址，*arr就是首元素，大小是1个字节
//	printf("%zd\n", sizeof(arr[1]));
//	// 1    arr[1]是第二个元素，大小是1个字节
//	printf("%zd\n", sizeof(&arr));
//	// 4/8  &arr是数组的地址，大小是4/8个字节
//	printf("%zd\n", sizeof(&arr + 1));
//	// 4/8  &arr是数组的地址，+1跳过整个数组以后还是地址，地址大小是4/8个字节
//	printf("%zd\n", sizeof(&arr[0] + 1));
//	// 4/8  &arr[0] + 1是第二个元素地址，是地址就是4/8个字节
//	return 0;
//}




//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "abcdef";
//	printf("%zd\n", strlen(arr));
//	// 6     计算\0之前的字符个数是6个
//	printf("%zd\n", strlen(arr + 0));
//	// 6     arr是首元素地址，arr + 0 还是首元素地址，\0直接的字符个数是6
//	printf("%zd\n", strlen(*arr));
//	// arr是首元素的地址，*arr是首元素,就是'a','a'的ASCII码值是97，就相当于把97
//	// 作为地址传给了strlen,strlen得到的就是野指针，所以会出错
//	printf("%zd\n", strlen(arr[1]));
//	// arr[1]是第二个元素'b'，会出错
//	printf("%zd\n", strlen(&arr));
//	// 6    &arr是数组的地址，也是数组首元素开始向后找，所以是6个字符
//	printf("%zd\n", strlen(&arr + 1));
//	// &arr + 1跳过了整个数组，随机值
//	printf("%zd\n", strlen(&arr[0] + 1));
//	// 5    &arr[0] + 1跳过了一个元素，所以结果是5
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	char* p = "abcdef";
//	printf("%zd\n", sizeof(p));
//	// 4/8   p是指针变量，大小是4/8个字节
//	printf("%zd\n", sizeof(p + 1));
//	// 4/8   p + 1是b的地址，地址大小为4/8个字节
//	printf("%zd\n", sizeof(*p));
//	// 1     p类型是char*，*p的类型是char,大小是1个字节
//	printf("%zd\n", sizeof(p[0]));
//	// 1     p[0] == *(p + 0) == *p == a ,大小是1个字节，也可以理解为首元素，大小是1个字节
//	printf("%zd\n", sizeof(&p));
//	// 4/8   取出的是p的地址，地址大小为4/8个字节
//	printf("%zd\n", sizeof(&p + 1));
//	// 4/8   &p + 1是跳过p指针变量后的地址，地址大小为4/8个字节
//	printf("%zd\n", sizeof(&p[0] + 1));
//	// 4/8   &p[0] + 1是第二个元素的地址，地址大小为4/8个字节
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char* p = "abcdef";
//	printf("%zd\n", strlen(p));
//	// 6   p中存放的是a的地址，strlen从前往后数，在\0之前，所以结果是6
//	printf("%zd\n", strlen(p + 1));
//	// 5   p + 1 指向的是b,从b往后数，\0之前，所以结果是5
//	printf("%zd\n", strlen(*p));
//	// *p就是'a','a'的ASCII码值是97，所以会出错
//	printf("%zd\n", strlen(p[0]));
//	// p[0] == *（p + 0）== *p，所以结果同上，会报错
//	printf("%zd\n", strlen(&p));
//	// &p是指针变量p的地址，和字符串"abcdef"关系不大，从p这个指针变量往后数，
//	// p中存放的地址不清楚，所以结果是随机值
//	printf("%zd\n", strlen(&p + 1));
//	// 与内存无关，同样是随机值
//	printf("%zd\n", strlen(&p[0] + 1));
//	// 5  从第二个元素开始往后数，长度是5
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a[3][4] = { 0 };
//	printf("%zd\n", sizeof(a));
//	// 48    a是数组名，数组名单独放在sizeof内部，计算的是数组的大小，单位是字节
//	//       3*4*4 = 48 
//	printf("%zd\n", sizeof(a[0][0]));
//	// 4     第一行第一个元素，大小是4个字节
//	printf("%zd\n", sizeof(a[0]));
//	// 16    第一行的数组名，数组名单独放在sizeof中，计算的是数组的总大小，大小是16个字节
//	printf("%zd\n", sizeof(a[0] + 1));
//	// 4/8   a[0]是第一行的数组名，但是a[0]没有单独放在sizeof中，所以这里的a[0]是
//	//       数组首元素的地址，就是&a[0][0]，a[0]+ 1是a[0][1]的地址，地址大小是4/8个字节
//	printf("%zd\n", sizeof(*(a[0] + 1)));
//	// 4     *(a[0] + 1)是第一行第二个元素，大小是4
//	printf("%zd\n", sizeof(a + 1));
//	// 4/8   a作为数组名并没有单独放在sizeof内部，a表示二维数组首元素的地址，
//	//       也就是第一行的地址，a + 1跳过一行，是第二行的地址，大小是4/8个字节
//	printf("%zd\n", sizeof(*(a + 1)));
//	// 16    a + 1是第二行的地址，*(a + 1)是第二行，计算的是第二行的大小，是16
//	printf("%zd\n", sizeof(&a[0] + 1));
//	// 4/8   &a[0]取出的是第一行的地址，&a[0] + 1是第二行的地址，大小是4/8个字节
//	printf("%zd\n", sizeof(*(&a[0] + 1)));
//	// 16    *(&a[0] + 1)对第二行地址解引用，是第二行的元素，大小是16个字节
//	printf("%zd\n", sizeof(*a));
//	// 16    a作为数组名并没有单独放在sizeof内部，a表示二维数组首元素的地址，
//	//       也就是第一行的地址，*a就是第一行，计算的是第一行的大小，是16
//	printf("%zd\n", sizeof(a[3]));
//	// 16    a[3]无需真实存在，通过类型推断就能算出长度，a[3]是第四行的数组名，
//	//       单独放在sizeof内部，计算的是第四行的大小，是16个字节
//	return 0;
//}