#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int main()
//{
//	int n = 0x11223344;
//	int* pi = &n;
//	*pi = 0;
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	int n = 10;
//	char* pc = (char*)&n;
//	int* pi = &n;
//	printf("%p\n", &n);
//	printf("%p\n", pc);
//	printf("%p\n", pc + 1);
//	printf("%p\n", pi);
//	printf("%p\n", pi + 1);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n = 10;
//	int* p = &n;
//	char* p1 = &n;
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	const n = 10;
//	int m = 0;
//	const m = 30;
//	return 0;
//}



//const放在*左边
//#include<stdio.h>
//int main()
//{
//	const int n = 10;
//	int m = 100;
//	const int* p = &n;//int const* p = &n;
//	p = &m;
//	return 0;
//}




//const放在*右边
//#include<stdio.h>
//int main()
//{
//	const int n = 10;
//	int m = 100;
//	p = &m;
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int age = 17;
//	if (age < 18)
//	{
//		printf("青年\n");
//	}
//	else if (age >= 18 && age < 38)
//	{
//		printf("壮年\n");
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", &arr[9] - &arr[0]);
//	printf("%d\n", &arr[0] - &arr[9]);
//	//指针 - 指针的绝对值是指针和指针之间的元素个数
//	return 0;
//}

//#include<stdio.h>
//int my_strlen(char*str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		str++;
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//#include<stdio.h>
//int my_strlen(char* p)
//{
//	char* p1 = p;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p - p1;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//	while (p < arr + sz)
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int* p;
//	*p = 20;
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int* p = &arr[0];
//	for (int i = 0; i <= 11; i++)
//	{
//		*(p++) = i;
//	}
//	return 0;
//}

//#include <stdio.h>
//int* test()
//{
//	int n = 100;
//	return &n;
//}
//int main()
//{
//	int* p = test();
//	printf("%d\n", *p);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int n = 10;
//	int* p1 = &n;
//
//	int* p = NULL;
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	if (p != NULL)
//	{
//		*p = 20;
//	}
//	printf("%d", a);
//	return 0;
//}
