#define _CRT_SECURE_NO_WARNINGS

//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	int ret = islower('a');
//	printf("%d\n", ret);
//	int result = islower('A');
//	printf("%d\n", result);
//	return 0;
//}


// 将字符串中的小写字母转化为大写
//#include<stdio.h>
//int main()
//{
//	char ch[] = "I am a stuDent";
//	int i = 0;
//	while (ch[i] != '\0')
//	{
//		if (ch[i] >= 'a' && ch[i] <= 'z')
//		{
//			ch[i] -= 32;
//		}
//		i++;
//	}
//	printf("%s\n", ch);
//	return 0;
//}

//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	char ch[] = "I am a stuDent";
//	int i = 0;
//	while (ch[i] != '\0')
//	{
//		if (islower(ch[i]))
//		{
//			ch[i] -= 32;
//		}
//		i++;
//	}
//	printf("%s\n", ch);
//	return 0;
//}

//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	char ch = 'a';
//	printf("%c\n", toupper(ch));
//	return 0;
//}

//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	char ch[] = "I am a stuDent";
//	int i = 0;
//	while (ch[i] != '\0')
//	{
//		if (islower(ch[i]))
//		{
//			ch[i] = toupper(ch[i]);
//		}
//		i++;
//	}
//	printf("%s\n", ch);
//	return 0;
//}



//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	if (strlen("abc") - strlen("abcdef") > 0)
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<=\n");
//	}
//	return 0;
//}


//strlen的模拟实现
//#include<stdio.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}


//#include<stdio.h>
//size_t my_strlen(const char* str)
//{
//	if (*str == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(str + 1);
//}
//int main()
//{
//	char arr[] = "abcdef";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}




// strcpy----字符串拷贝
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = { 0 };
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

// strcpy的模拟实现
//#include<stdio.h>
//#include<assert.h>
//void my_strcpy(char *dest,const char *src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	//拷贝\0前面的内容
//	while (*src!='\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	//拷贝\0
//	*dest = *src;
//
//}
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = { 0 };
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//#include<stdio.h>
//#include<assert.h>
//void my_strcpy(char* dest, const char* src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	//拷贝\0前面的内容
//	while (*src != '\0')
//	{
//		*dest++ = *src++;
//	}
//	//拷贝\0
//	*dest = *src;
//
//}
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = { 0 };
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//#include<stdio.h>
//#include<assert.h>
//void my_strcpy(char* dest, const char* src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	while (*dest++ = *src++)
//	{
//		;
//	}
//
//}
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = { 0 };
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}


// 本部分最优的代码
//#include<stdio.h>
//#include<assert.h>
//char* my_strcpy(char* dest, const char* src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = { 0 };
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}


//strcat----连接（追加）字符串
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}


//模拟实现strcat
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//char* my_strcat(char* dest, const char* src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	//找到目标空间的\0
//	char *ret = dest;
//	while (*dest != '\0')
//	{
//		dest++;
//	}
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//void Func(char src[], int k)
//{
//	int len = strlen(src);
//	int time = k % len;
//	int i = 0;
//	for (i = 0; i < time; i++)
//	{
//		char tmp = src[0];
//		int j = 0;
//		for (j = 0; j < len - 1; j++)
//		{
//			src[j] = src[j + 1];
//		}
//		src[j] = tmp;
//	}
//}
//int main()
//{
//	char str[] = "abcd";
//	int k = 0;
//	scanf("%d", &k);
//	Func(str, k);
//	printf("%s\n", str);
//	return 0;
//}



//strncat----->拼接n个字符
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str1[] = "abcd";
//	char str2[] = "hhhh";
//	strncat(str2, str1,2);
//	// str2后面拼接str1中的n个字符
//	printf("%s\n", str2);
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//void Func(char src[], int k)
//{
//	int len = strlen(src);
//	int time = k % len;
//	char tmp[200] = { 0 };
//	strcpy(tmp, src + time);
//	strncat(tmp, src, time);
//	strcpy(src, tmp);
//}
//int main()
//{
//	char str[] = "abcd";
//	Func(str, 2);
//	printf("%s\n", str);
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//void reverse(char str[], int i, int j)
//{
//	while (i < j)
//	{
//		char tmp = str[i];
//		str[i] = str[j];
//		str[j] = tmp;
//		i++;
//		j--;
//	}
//}
//void Func(char src[], int k)
//{
//	int len = strlen(src);
//	int time = k % len;
//	reverse(src, 0, time - 1);
//	reverse(src, time, len - 1);
//	reverse(src, 0, len - 1);
//}
//int main()
//{
//	char str[] = "abcd";
//	Func(str, 2);
//	printf("%s\n", str);
//	return 0;
//}

