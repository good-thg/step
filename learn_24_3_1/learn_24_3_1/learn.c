#define _CRT_SECURE_NO_WARNINGS
//字符指针变量
//#include<stdio.h>
//int main()
//{
//	char ch = 'm';
//	char* pc = &ch;
//	*pc = 'm';
//	printf("%c", ch);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	const char* p = "hello world";
//	//常量字符串不能修改
//  //常量字符串存储在一份中
//	printf("%c\n", *p);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	const char* p = "hello world";
//	printf("%s\n", p);
//	printf("%s\n", "hello world");
//	int len = strlen(p);
//	for (int i = 0; i < len; i++)
//	{
//		printf("%c", *(p + i));
//	}
//	return 0;
//}
//数组指针变量
//数组指针是一种指针变量，是存放数组的指针变量
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int (*p)[10] = &arr;
//	//p是数组指针，p指向的是数组，数组10个元素，每个元素的类型的int
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char* arr[5];
//	char (*p)[5] = &arr;
//  char (*)[5]是数组指针类型
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int(*p)[10] = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", (*p)[i]);
//	}
//	return 0;
//}
//二维数组传参的本质
//#include<stdio.h>
//void test(int arr[3][5], int r,int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
//	test(arr, 3, 5);
//	return 0;
//}


//#include<stdio.h>
//void test(int (*p)[5], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			//printf("%d ", (*(p + i))[j]);
//			printf("%d ", *(*(p + i)+j));
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
//	test(arr, 3, 5);
//	return 0;
//}
//函数指针
//指向的是函数，存放的是函数的地址
//#include<stdio.h>
//int Add(int a, int b)
//{
//	return a + b;
//}
//int main()
//{
//	int x = 10;
//	int y = 20;
//	int c = Add(x, y);
//	printf("%d\n", c);
//	printf("%p\n", Add);
//	printf("%p\n", &Add);
//	//&函数名和函数名都表示函数的地址
//	return 0;
//}

//#include<stdio.h>
//int Add(int a, int b)
//{
//	return a + b;
//}
////函数指针变量的写法和数组指针变量的写法类似
//
//int main()
//{
//	int (*pf1)(int, int) = &Add;
//	int (*pf2)(int, int) = Add;
//	int r1 = (*pf1)(3, 7);
//	int r2 = pf1(3, 7);
//
//	printf("%d\n", r1);
//	printf("%d\n", r2);
//
//	return 0;
//}
//typedef关键字
//用来类型重命名，可以将复杂的类型简单化


//typedef unsigned int unit;
//#include<stdio.h>
//int main()
//{
//	unit num;
//
//	return 0;
//}


//typedef int* pint;
//#include<stdio.h>
//int main()
//{
//	int* p1 = 0;
//	pint p2 = 0;
//	return 0;
//}