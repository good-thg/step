#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//int main()
//{
//	short a = 2;
//	int b = 10;
//	printf("%zd\n", sizeof(a = b + 1));
//	printf("a=%d\n", a);
//	//最终的结果两个都是2
//	//放在sizeof内部的表达式不会真的计算，它只是根据计算最后的类型决定的
//	return 0;
//}

//#include<stdio.h>
//void swap(int* px, int* py)
//{
//	int t = 0;
//	t = *px;
//	*px=*py;
//	*py = t;
//	
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	//输入三个值
//	scanf("%d %d %d", &a, &b, &c);
//	//调整
//	if (a < b)
//	{
//		swap(&a, &b);
//	}
//	if (a < c)
//	{
//		swap(&a, &c);
//	}
//	if (b < c)
//	{
//		swap(&b, &c);
//	}
//	//输出
//	printf("%d %d %d", a, b, c);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	//输入三个值
//	scanf("%d %d %d", &a, &b, &c);
//	//调整
//	int t = 0;
//	if (a < b)
//	{
//	    t = a;
//		a = b;
//		b = t;
//	}
//	if (a < c)
//	{
//		t = a;
//		a = c;
//		c = t;
//	}
//	if (b < c)
//	{
//		t = b;
//		b = c;
//		c = t;
//	}
//	//输出
//	printf("%d %d %d", a, b, c);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	//*p 等价于 a
//	*p = 100;
//	printf("%d", a);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	char* pc = &a;
//	printf("%p\n", pa);
//	printf("%p\n", pa + 1);
//	printf("%p\n", pc);
//	printf("%p\n", pc + 1);
//	return 0;
//}
// const修饰变量
//#include<stdio.h>
//int main()
//{
//	int m = 0;
//	m = 20;
//	const int n = 10;
//	//const 是常属性的意思，不能被修改
//  //n 还是变量，n 叫做常变量
//	n = 20;
//	printf("%d", n);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	const int n = 10;
//	//int arr[n] = { 0 };
//	int* p = &n;
//	*p = 20;
//	printf("%d", n);
//	return 0;
//}
// const 修饰指针变量
// 1.const 放在*左边
// 修饰的是 *p ，意思是不能通过p来改变p指向的对象的内容，但是p本身是可以改变的，p可以指向其他对象
// 2.const 放在*右边
// 限制的是p,意思是不能修改p本身的值，但是p指向的内容是可以改变的
//#include<stdio.h>
//int main()
//{
//	const int n = 10;
//	//左边
//	const int* p = &n;
//    int const * p = &n;
//	//右边
//    int* const  p = &n;
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int* p = &arr[0];
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//		//printf("%d ", *p);
//		//p++;
//	}
//	return 0;
//}
// 指针-指针，运算前提是两个指针指向了同一块空间
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", &arr[0] - &arr[9]);
//	printf("%d\n", &arr[9] - &arr[0]);
// // 指针-指针的绝对值是指针和指针之间的元素个数
//	return 0;
//}
// 模拟实现strlen
//int my_strlen(char* p)
//{
//	int count = 0;
//	while (*p != '\0')
//	{
//		count++;
//		p++;
//	}
//	return count;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//int my_strlen(char* p)
//{
//	char* p1 = p;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p-p1;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

#include<stdio.h>
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int* p = &arr[0];
	while (p < arr + sz)
	{
		printf("%d ", *p);
		p++;
	}
	return 0;
}