#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//void print_arr(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz);
//	print_arr(arr, sz);
//	return 0;
//}
//



//#include<stdio.h>
//#include<stdlib.h>
//void print_arr(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//int compar_int(const void* p1, const void* p2)
//{
//	if (*(int*)p1 > *(int*)p2)
//	{
//		return 1;
//	}
//	else if (*(int*)p1 == *(int*)p2)
//	{
//		return 0;
//	}
//	else
//	{
//		return -1;
//	}
//}
//void test1()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), compar_int);
//	print_arr(arr, sz);
//}
//int main()
//{
//	test1();
//	return 0;
//}


//升序
//#include<stdio.h>
//#include<stdlib.h>
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//int compar_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//void test1()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), compar_int);
//	print_arr(arr, sz);
//}
//int main()
//{
//	test1();
//	return 0;
//}

//降序
//#include<stdio.h>
//#include<stdlib.h>
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//int compar_int(const void* p1, const void* p2)
//{
//	return *(int*)p2 - *(int*)p1;
//}
//void test1()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), compar_int);
//	print_arr(arr, sz);
//}
//int main()
//{
//	test1();
//	return 0;
//}


// 结构体成员访问操作符
//  结构体变量.成员名      直接访问
//  结构体指针->成员名     间接访问

//#include<stdio.h>
//struct Stu
//{
//	char name[20];
//	int age;
//};
//int main()
//{
//	struct Stu s = { "Tom",12 };
//	printf("%s %d", s.name, s.age);
//	return 0;
//}

//#include<stdio.h>
//struct Stu
//{
//	char name[20];
//	int age;
//};
//void print(struct Stu* ps)
//{
//	printf("%s %d\n", (*ps).name, (*ps).age);
//	printf("%s %d\n", ps->name, ps->age);
//}
//int main()
//{
//	struct Stu s = { "Tom",12 };
//	print(&s);
//	return 0;
//}


//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//struct Stu
//{
//	char name[20];
//	int age;
//};
//int compar_stu_by_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
//int compar_stu_by_age(const void* p1, const void* p2)
//{
//	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
//}
//// name
//void test1()
//{
//	struct Stu s[] = {{"LiHua",18},{"Sam",22},{"Tom",16}};
//	int sz = sizeof(s) / sizeof(s[0]);
//	qsort(s, sz, sizeof(s[0]), compar_stu_by_name);
//}
//// age
//void test2()
//{
//	struct Stu s[] = { {"Lihua",18},{"Sam",22},{"Tom",16} };
//	int sz = sizeof(s) / sizeof(s[0]);
//	qsort(s, sz, sizeof(s[0]), compar_stu_by_age);
//}
//int main()
//{
//	test1();
//	test2();
//	return 0;
//}




//#include<stdio.h>
//#include<string.h>
//int compar_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//void Swap(char* buf1, char* buf2, size_t width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//void bubble_sort(void * base,size_t sz,size_t width,int (*cmp)(const void*p1,const void*p2))
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base+j*width,(char*)base+(j+1)*width)>0)
//			{
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
//			}
//		}
//	}
//}
//void print_arr(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
////整形数据
//void test1()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), compar_int);
//	print_arr(arr, sz);
//}
////结构体
//struct Stu
//{
//	char name[20];
//	int age;
//};
//// age
//int compar_stu_by_age(const void* p1, const void* p2)
//{
//	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
//}
//void test2()
//{
//	struct Stu s[] = { {"Lihua",18},{"Sam",22},{"Tom",16} };
//	int sz = sizeof(s) / sizeof(s[0]);
//	bubble_sort(s, sz, sizeof(s[0]), compar_stu_by_age);
//}
////name
//int compar_stu_by_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
//void test3()
//{
//	struct Stu s[] = { {"LiHua",18},{"Sam",22},{"Tom",16} };
//	int sz = sizeof(s) / sizeof(s[0]);
//	bubble_sort(s, sz, sizeof(s[0]), compar_stu_by_name);
//}
//int main()
//{
//	test1();
//	test2();
//	test3();
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	printf("%zd\n", sizeof(int));
//	printf("%zd\n", sizeof(a));
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[3] = { 'a','b','c' };
//	char arr2[] = "abc";
//	printf("%zd\n", strlen(arr1));
//	printf("%zd\n", strlen(arr2));
//
//	printf("%zd\n", sizeof(arr1));
//	printf("%zd\n", sizeof(arr2));
//	return 0;
//}

#include<stdio.h>
int main()
{
	int a[] = { 1,2,3,4 };
	printf("%zd\n", sizeof(a));
	// 16    sizeof(数组名)的场景，在C语言指针（2）---数组名的理解中有说明过
	printf("%zd\n", sizeof(a + 0));
	// 4/8   a是首元素的地址，类型是 int * ，a + 0 还是首元素的地址，是地址就是 4/8 个字节
	printf("%zd\n", sizeof(*a));
	// 4     a 是首元素地址，*a 就是首元素，大小就是4个字节
	// *a == a[0] == *(a + 0)
	printf("%zd\n", sizeof(a + 1));
	// 4/8   a是首元素的地址，类型是 int * ，a + 1 跳过1个整形，a + 1 就是第二个元素的地址
	//       大小就是 4/8 个字节
	printf("%zd\n", sizeof(a[1]));
	// 4     a[1] 就是第二个元素，大小是4个字节
	printf("%zd\n", sizeof(&a));
	// 4/8   &a 是数组的地址，是地址大小就是 4/8 个字节
	printf("%zd\n", sizeof(*&a));
	// 16
	// 两种理解方式：
	// 1、*& 互相抵消了，sizeof(&a) == sizeof(a)
	// 2、&a 是数组的地址，类型是 int(*)[4]，对数组解引用访问的是数组，计算的是数组的大小
	printf("%zd\n", sizeof(&a + 1));
	// 4/8    &a + 1 是跳过整个数组后的那个位置的地址，大小是 4/8 个字节
	printf("%zd\n", sizeof(&a[0]));
	// 4/8    数组首元素的地址，大小是 4/8 个字节
	printf("%zd\n", sizeof(&a[0] + 1));
	// 4/8    数组第二个元素的地址，大小是 4/8 个字节
	return 0;
}
