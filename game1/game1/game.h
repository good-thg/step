#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define ROW 3
#define COL 3
// 菜单
void menu();
// 初始化棋盘
void InitBoard(char board[ROW][COL], int row, int col);
// 打印棋盘
void DisPlayBoard(char board[ROW][COL], int row, int col);
// 下棋
// 玩家下棋
void Playermove(char board[ROW][COL], int row, int col);
// 电脑下棋
void Computermove(char board[ROW][COL], int row, int col);
// 判断输赢
// 当玩家赢的时候，返回 '*'
// 当电脑赢的时候，返回 '#'
// 平局，返回'Q'
// 继续，返回'C'
char IsWin(char board[ROW][COL], int row, int col);