#define _CRT_SECURE_NO_WARNINGS


//#include<stdio.h>
//#include<errno.h>
//#include<string.h>
//int main()
//{
//	FILE* pf = fopen("learn.txt", "r");   // 打开文件
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	fclose(pf);                           // 关闭文件
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	FILE* pf = fopen("learn.txt", "r");   
//	if (pf == NULL)
//	{
//		perror("problem");
//		return 1;
//	}
//	fclose(pf);                           
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//char* my_strncat(char* dest, const char* src, size_t n) 
//{
//    size_t len = strlen(dest);
//    int i = 0;
//    for (i = 0; i < n && src[i] != '\0'; i++) 
//    {
//        dest[len + i] = src[i];
//    }
//    dest[len + i] = '\0';
//    return dest;
//}
//int main() {
//    char str1[20] = "hello ";
//    char str2[] = "world";
//    my_strncat(str1, str2, 3);
//    printf("%s\n", str1);
//    return 0;
//}



//char* mystrncpy(char* dst, const char* src, size_t n)
//{
//    int i;
//    for (i = 0; src[i] && i < n; i++)
//    {
//        dst[i] = src[i];
//    }
//
//    if (i < n)
//    {
//        dst[i] = 0;
//    }
//    return dst;
//}


//char* mystrncat(char* dst, const char* src, size_t n)
//{
//    char* tmp = dst;
//
//    while (*dst)
//    {
//        dst++;
//    }
//
//    int i;
//    for (i = 0; src[i] && i < n; i++)
//    {
//        dst[i] = src[i];
//    }
//
//    dst[i] = 0;
//    return tmp;
//}


//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 0 };
//	memcpy(arr2, arr1, 20);   //  20 = 5*sizeof(int)  以字节为单位
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//#include <stdio.h>
//#include<assert.h>
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		src = (char*)src + 1;
//		dest = (char*)dest + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 0 };
//	my_memcpy(arr2, arr1, 20); 
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}


//#include <stdio.h>
//#include<assert.h>
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	if (dest < src)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			src = (char*)src + 1;
//			dest = (char*)dest + 1;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "hello world";
//	memset(arr+2, 'x', 5);
//	printf("%s\n", arr);
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr[5] = { 0 };
//	memset(arr, 1, 20);
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char ptr1[] = "abcdef";
//	char ptr2[] = "abcfgh";
//	int ret = memcmp(ptr1, ptr2, 5);
//	printf("%d\n", ret);
//	return 0;
//}





//#include<stdio.h>
//int main()
//{
//	int n = 0x11223344;
//	return 0;
//}


//#include<stdio.h>
//int check_sys()
//{
//	int n = 1;
//	return (*(char*)&n);
//}
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int h = 0;
//	int m = 0;
//	while (scanf("%d %d %d", &n, &h, &m) != EOF)
//	{
//		if (m % h)
//		{
//			printf("%d\n", n - (m / h) - 1);
//		}
//		else
//		{
//			printf("%d\n", n - (m / h));
//		}
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	double price = 0.0;
//	int month = 0;
//	int day = 0;
//	int flag = 0;
//	double cut = 1.0;
//	double last = 0.0;
//	scanf("%lf %d %d %d", &price, &month, &day, &flag);
//	if (month == 11 && day == 11)
//	{
//		cut = 0.7;
//		if (flag == 1)
//		{
//			last = price * cut - 50;
//		}
//		else
//		{
//			last = price * cut;
//		}
//	}
//	else if (month == 12 && day == 12)
//	{
//		cut = 0.8;
//		if (flag == 1)
//		{
//			last = price * cut - 50;
//		}
//		else
//		{
//			last = price * cut;
//		}
//	}
//	if (last < 0.0)
//	{
//		printf("%.2lf\n", 0.0);
//	}
//	else
//	{
//		printf("%.2lf\n", last);
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	double price = 0.0;
//	int month = 0;
//	int day = 0;
//	int flag = 0;
//	scanf("%lf %d %d %d", &price, &month, &day, &flag);
//	if (month == 11 && day == 11)
//	{
//		price *= 0.7;
//		if (flag == 1)
//		{
//			price -= 50;
//		}
//	}
//	else if (month == 12 && day == 12)
//	{
//		price *= 0.8;
//		if (flag == 1)
//		{
//			price -= 50;
//		}
//	}
//	if (price < 0.0)
//	{
//		printf("%.2lf\n", 0.0);
//	}
//	else
//	{
//		printf("%.2lf\n", price);
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	double price = 0.0;
//	int month = 0;
//	int day = 0;
//	int flag = 0;
//	double cut = 1.0;
//	double last = 0.0;
//	scanf("%lf %d %d %d", &price, &month, &day, &flag);
//	if (month == 11 && day == 11)
//	{
//		cut = 0.7;
//	}
//	else if (month == 12 && day == 12)
//	{
//		cut = 0.8;
//	}
//	last = price * cut - flag * 50;
//	if (last < 0.0)
//	{
//		printf("%.2lf\n", 0.0);
//	}
//	else
//	{
//		printf("%.2lf\n", last);
//	}
//	return 0;
//}
//