#define _CRT_SECURE_NO_WARNINGS


//#include <stdio.h>
//unsigned char i = 0;
//int main()
//{
//	for (i = 0; i <= 255; i++)
//	{
//		printf("hello world\n");
//	}
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	unsigned int i;
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//	}
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int a[4] = { 1, 2, 3, 4 };
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);
//	printf("%x,%x", ptr1[-1], *ptr2);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int n = 9;
//	float* pFloat = (float*)&n;
//	printf("n的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	*pFloat = 9.0;
//	printf("num的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	return 0;
//}



//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    int ret = 1;
//    int sum = 0;
//    for (i = 1; i <= n; i++)
//    {
//        ret *= i;
//        sum += ret;
//    }
//    printf("%d", sum);
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    int ret = 1;
//    int sum = 0;
//    if (n >= 1 && n <= 50)
//    {
//        for (i = 1; i <= n; i++)
//        {
//            ret *= i;
//            sum += ret;
//        }
//    }
//    printf("%d", sum);
//    return 0;
//}



// 自定义类型------>结构体
//#include<stdio.h>
//struct Node
//{
//	int date;
//	struct Node* next;
//};
//int main()
//{
//
//	return 0;
//}


//#include<stdio.h>
//struct stu
//{
//	int num;
//	char name[10];
//	int age;
//};
//
//void fun(struct stu* p)
//{
//	printf("%s\n", (*p).name);
//	return;
//}
//
//int main()
//{
//	struct stu students[3] = { {9801,"zhang",20},
//							 {9802,"wang",19},
//					         {9803,"zhao",18} };
//	fun(students + 1);
//	return 0;
//}

//#include < stdio.h >
//struct S
//{
//	int a;
//	int b;
//};
//int main()
//{
//	struct S a, * p = &a;
//	a.a = 99;
//	printf("%d\n", a.a);
//	return 0;
//}




// 位段
// 用于节省空间
//#include<stdio.h>
//struct s
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct s));
//	return 0;
//}


//#include<stdio.h>
//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//int main()
//{
//	struct S s = { 0 };
//	s.a = 10;
//	s.b = 12;
//	s.c = 3;
//	s.d = 4;
//	return 0;
//}


// 联合体
//#include <stdio.h>
//union Un
//{
//	char c;
//	int i;
//};
//int main()
//{
//	union Un un = { 0 };
//	printf("%zd\n", sizeof(un));
//	return 0;
//}

//#include<stdio.h>
//int check()
//{
//	int n = 1;
//	return *(char*)&n;
//}
//int main()
//{
//	int ret = check();
//	if (ret == 1)
//		printf("small");
//	else
//		printf("big");
//	return 0;
//}


//#include<stdio.h>
//int check()
//{
//	union Un
//	{
//		char c;
//		int i;
//	}u;
//	u.i = 1;
//	return u.c;
//}
//int main()
//{
//	int ret = check();
//	if (ret == 1)
//		printf("small");
//	else
//		printf("big");
//	return 0;
//}

//#include<stdio.h>
//#include<Windows.h>
//int main()
//{
//	system("color 0c");
//	printf("*********");
//	return 0;
//}

