#define _CRT_SECURE_NO_WARNINGS
// 逗号表达式
//#include<stdio.h>
//int main()
//{
//	int a = 1;
//	int b = 2;
//	int c = (a > b, a = b + 10, a, b = a + 1);
//	printf("%d", c);
//	return 0;
//}


//Python学习
//# 字符串列表
//# encode 编码：将字符转换成字节流（二进制数据流）
//a = 'hello'
//b = b'hello'
//print(type(b))    # <class 'bytes'>  字节码，也叫字节流
//a = 'hello'
//a1 = a.encode()  # 给 a 进行编码，把字符串转换成二进制数据流
//print(a1)
//# decode 解码：将字节流解析为字符（还原字符串）
//a2 = a1.decode()
//print(a2)
//print(type(a2))
//
//# 字符串切片
//# 切片语法[起始索引:结束索引:步长]
//# 前闭后开区间
//name = 'class_name'
//b = name[2:6 : 2]
//print(b)
//print(name[-1:3 : -1])  # 逆序输出
//
//
//# 字符串的查找
//# 1.find  字符串序列.find(子串，开始下标，结束下标)
//# 检测子串是否包含在原字符串中，如果在则返回第一次找到的索引值，不在则返回 - 1
//a = 'hello world'
//print(a.find('e'))
//# 2.index  字符串序列.index(子串，开始下标，结束下标)
//# 如果不在则会报错
//a = 'hello world'
//print(a.index('e'))
//# 3.count  字符串序列.count(子串，开始下标，结束下标)
//# 查找子串在原字符串中出现的次数
//a = 'hello world'
//print(a.count('l'))
//
//# 字符串的修改
//# 1.replace  字符串序列.replace(旧子串，新子串，替换次数)
//a = 'hello world'
//print(a.replace('l', 'a', 2))
//# 2.split 分割  a.split（str, 切的次数） 用str来切原字符串
//a = 'he,llo wor,ld'
//print(a.split(','))
//
//# capitalize 首字母大写
//a = 'hello world'
//print(a.capitalize())
//
//# islower()  判断是否全部由小写字母组成
//str = 'helllo'
//print(str.islower())
//# isupper() 同理
//j = 0
//str = '123123'
//for i in str :
//if str.isdigit() :
//    j += 1
//    print(j)
//
//
//    # startswith  是否以某字符开头
//    # endswith    是否以某字符结束
//    a = 'hello world'
//    print(a.startswith('h'))
//    print(a.endswith('d'))
//
//
//    # 字符串的增删改查
//    # 一、增
//    # 1. +
//    str1 = 'hello'
//    str2 = 'world'
//    info = str1 + str2
//    print(info)
//    # 2.join
//    str1 = 'hello'
//    print('*'.join(str1))  # h * e * l * l * o
//
//    # 二、删
//    # 1.lstrip()  删除字符串左边的空白字符
//    a = ' hello'
//    print(a)
//    print(a.lstrip())
//    # rstrip()  删除右边的空白
//    # strip()   删除字符串两端的空白字符


//numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
//print(min(numbers))
//print(max(numbers))
//print(sum(numbers))
//
//squares = [value * *2 for value in range(1, 11)]
//print(squares)
//
//
//players = ['charles', 'martina', 'michael', 'florence', 'eli']
//print(players[-3:])

