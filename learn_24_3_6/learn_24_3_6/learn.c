#define _CRT_SECURE_NO_WARNINGS
// Python 代码学习
//# 删除空白
//n = 'python '
//print(n)
//n = n.rstrip()
//print(n)
//
//# 删除前缀
//m = 'http://www.Thg.cn,com'
//print(m)
//m = m.removeprefix('http://')
//print(m)
//
//# 在列表中添加元素
//# 1.append()方法，添加在列表末尾
//list = ['sam', 'tom', 'zhangsan']
//list.append('lisi')
//print(list)
//# 2.insert()方法在列表中插入元素
//# 这种操作将了列表中每个既有元素都右移一个位置
//list = ['sam', 'tom', 'zhangsan']
//list.insert(2, 'wangwu')
//print(list)
//
//# 删除元素
//# 1.del语句删除元素
//list = ['sam', 'tom', 'zhangsan']
//del list[1]
//print(list)
//# 2.pop()方法
//list = ['sam', 'tom', 'zhangsan']
//list.pop()
//print(list)  # 在这里是删除了末尾的元素
//
//list = ['sam', 'tom', 'zhangsan']
//list.pop(1)
//print(list)  # 删除中间元素
//
//# 3.根据值来删除元素
//# remove()方法，删除后还可以使用
//list = ['sam', 'tom', 'zhangsan']
//list.remove('tom')
//print(list)
//
//# 管理列表
//# sort() 按字母顺序排列
//list = ['sam', 'zhangsan', 'tom']
//list.sort()
//print(list)
//# 按照字母顺序相反排列
//list = ['sam', 'zhangsan', 'tom']
//list.sort(reverse = True)
//print(list)
//# 这两种方法对列表的修改是永久的
//
//# 使用 sorted() 函数对列表进行临时排序
//list = ['sam', 'zhangsan', 'tom']
//print(sorted(list))
//print(list)
//
//# 反向打印列表
//# 使用 reverse() 方法
//# reverse() 不是按与字母顺序相反的顺序排列列表元素，只是反转列表元素
//# 的排列顺序
//cars = ['bmw', 'audi', 'toyota', 'subaru']
//print(cars)
//cars.reverse()
//print(cars)

//#include <math.h>
//int main() {
//    float m = 0.0;
//    char b = 0;
//    int money = 20;
//    scanf("%f %c", &m, &b);
//    if (m > 0 && m <= 1) {
//        if (b == 'y')
//            printf("25");
//        else
//            printf("20");
//    }
//    if (m > 1) {
//        money = money + ceil(m - 1);
//        if (b == 'y')
//            printf("%d", money + 5);
//        else
//            printf("%d", money);
//    }
//
//
//}



//#include<stdio.h>
//int main() {
//    double a = 0.0;
//    double b = 0.0;
//    char op = 0;
//    scanf("%lf %c %lf", &a, &op, &b);
//    if (op == '+') {
//        printf("%.4lf+%.4lf=%.4lf", a, b, a + b);
//    }
//    else if (op == '-') {
//        printf("%.4lf-%.4lf=%.4lf", a, b, a - b);
//    }
//    else if (op == '*') {
//        printf("%.4lf+%.4lf=%.4lf", a, b, a * b);
//    }
//    else if (op == '/') {
//        if (b == 0) {
//            printf("Wrong!Division by zero!");
//        }
//        else {
//            printf("%.4lf/%.4lf=%.4lf", a, b, a / b);
//        }
//    }
//    else {
//        printf("Invalid operation!");
//    }
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//    double a, b;
//    char ch;
//    while (scanf("%lf %c %lf", &a, &ch, &b) != EOF)
//    {
//        if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
//        {
//            if (ch == '+')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a + b);
//            else if (ch == '-')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a - b);
//            else if (ch == '*')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a * b);
//            else
//            {
//                if (b == 0.0)
//                    printf("Wrong!Division by zero!\n");
//                else
//                    printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a / b);
//            }
//        }
//        else
//            printf("Invalid operation!\n");
//    }
//    return 0;
//}



//#include<stdio.h>
//int main() {
//    int n = 0;
//    while (scanf("%d", &n) != EOF) {
//        for (int i = 1; i <= n; i++) {
//            for (int j = 1; j <= i; j++) {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    while (n)
//    {
//        printf("%d", n % 10);
//        n /= 10;
//    }
//    return 0;
//}