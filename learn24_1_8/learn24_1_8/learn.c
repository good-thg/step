#define _CRT_SECURE_NO_WARNINGS
//PTA
#include<stdio.h>
#include<math.h>
int main()
{
    double eps;
    double n = 0.0;
    scanf("%lf", &eps);//最近总是忘记这一句
    double fm = 1.0;
    int flag = 1;
    double sum = 0.0;
    do
    {
        n = 1.0 / fm;
        sum += flag * n;
        fm += 2;
        flag = -flag;

    } while (fabs(n) >= eps);//do-while循环while语句后面需要加分号
                             //fabs()需要引头文件#include<math.h>
      double pi = 4*sum;
    printf("Pi = %.4lf", pi);
    return 0;
}
#include<stdio.h>
#include<math.h>
int main()
{
    int m = 0;
    int n = 0;
    int i = 0;
    int x = 0;
    int y = 0;
    int z = 0;
    scanf("%d %d", &m, &n);
    if (m >= 100 && m <= n && n <= 999)
    {
        for (i = m; i <= n; i++)
        {
            x = i / 100;
            y = i / 10 % 10;
            z = i % 10;
            if (i == pow(x, 3) + pow(y, 3) + pow(z, 3))
            {
                printf("%d\n", i);
            }
        }
    }
    else
        printf("Invalid Value.");
    return 0;
}
#include<stdio.h>
#include<ctype.h>
int main()
{
    int letter = 0;
    int blank = 0;
    int digit = 0;
    int other = 0;
    char ch;
    int i;
    for (i = 1; i <= 10; i++)
    {
        scanf("%c", &ch);
        if (isalpha(ch))
        {
            letter++;
        }
        else if (ch == ' ' || ch == '\n')
        {
            blank++;
        }
        else if (isdigit(ch))
        {
            digit++;
        }
        else
        {
            other++;
        }
    }
    printf("letter = %d, blank = %d, digit = %d, other = %d", letter, blank, digit, other);
    return 0;
}
//         头文件----------->#include<ctype.h>
// 判断是否是字母----------->isalpha()
//           大写----------->isupper()
//           小写----------->islower()
//           小写--->大写    toupper()
//           大写--->小写    tolower()
//           数字----------->isdigit()
//
#include<stdio.h>
#include<math.h>
int main()
{
    double eps;
    double n = 0.0;
    scanf("%lf", &eps);
    double fm = 1.0;
    int flag = 1;
    double sum = 0.0;
    do
    {
        n = 1.0 / fm;
        sum += flag * n;
        fm += 2;
        flag = -flag;

    } while (fabs(n) >= eps);
    double pi = 4 * sum;
    printf("Pi = %.4lf", pi);
    return 0;
}
#include<stdio.h>
int main()
{
	int n, count = 0, i, j;
	scanf("%d", &n);
	for (i = 2; i <= n; i++)
	{
		for (j = 2; j < i; j++) {
			if (i % j == 0) 
			break;
		}
		if (j >= i) {
			printf("%6d", i);
			count++;
			if (count % 10 == 0) 
			printf("\n");
		}
	}
	return 0;
}
#include<stdio.h>
int main()
{
    int i = 0;
    int n = 0;
    scanf("%d", &n);
    int k = 0;
    for (i = 2; i <= n; i++)
    {
        int j = 0;
        for (j = 2; j <= i; j++)
        {
            if (i % j == 0)
            {
                break;
            }
        }
        if (i == j)
        {
            printf("%6d", i);
            k++;
            if (k % 10 == 0)
            {
                printf("\n");
            }
        }
    }
    if (k % 10 != 0)
    {
        printf("\n");
    }
    return 0;
}
#include<stdio.h>
int main()
{
    int number = 0;
    int n = 0;//次数
    scanf("%d %d", &number, &n);
    int i = 0;
    int guess = 0;
    for (i = 1; i <= n; i++)
    {
        scanf("%d", &guess);
        if (guess < 0)
        {
            printf("Game Over\n");
            break;
        }
        if (guess < number)
        {
            printf("Too small\n");
        }
        if (guess > number)
        {
            printf("Too big\n");
        }
        if (guess == number)
        {
            if (i == 1)
            {
                printf("Bingo!\n");
            }
            if (i > 1 && i <= 3)
            {
                printf("Lucky You!\n");
            }
            if (i > 3 && i <= n)
            {
                printf("Good Guess!\n");
            }
            break;
        }
        if (i == n)
        {
            printf("Game Over\n");
            break;
        }
    }
    return 0;
}
#include<stdio.h>
int main()
{
    int x = 0;
    scanf("%d", &x);
    int count = 0;
    int total = 0;
    int fen1 = 0;
    int fen2 = 0;
    int fen5 = 0;
    for (fen5 = x / 5; fen5 > 0; fen5--)
        for (fen2 = x / 2; fen2 > 0; fen2--)
            for (fen1 = x / 1; fen1 > 0; fen1--)
            {
                if (x == fen5 * 5 + fen2 * 2 + fen1 * 1)
                {
                    count++;
                    total = fen5 + fen2 + fen1;
                    printf("fen5:%d, fen2:%d, fen1:%d, total:%d\n", fen5, fen2, fen1, total);
                }
            }
    printf("count = %d", count);
    return 0;
}
#include<stdio.h>
int main()
{
    int n = 0;
    int max = 0;
    int b = 0;
    scanf("%d", &n);
    int arr[10];
    scanf("%d", &arr[0]);
    max = arr[0];
    for (int i = 1; i < n; i++)
    {
        scanf("%d", &arr[i]);
        if (arr[i] > max)
        {
            max = arr[i];
            b = i;
        }
    }
    printf("%d %d", max, b);
    return 0;
}
#include<stdio.h>
void  swap(int* x, int* y)
{
    int z = 0;
    z = *x;
    *x = *y;
    *y = z;

}
int main()
{
    int a = 0;
    int b = 0;
    scanf("%d %d", &a, &b);
    swap(&a, &b);
    printf("After swap:%d,%d", a, b);
    return 0;
}
#include<stdio.h>
int main()
{
    int n = 0;
    int arr[10];
    int sum = 0;
    scanf("%d", &n);
    int i = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%d ", &arr[i]);
        if (arr[i] % 2 == 0)
        {
            sum += arr[i];
        }
    }
    printf("%d\n", sum);
    return 0;
}
#include<stdio.h>
int main()
{
    int n = 0;
    int arr[100];
    scanf("%d", &n);
    int i = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }
    int j = 0;
    for (j = n - 1; j >= 0; j--)
    {
        printf("%d ", arr[j]);
    }
    return 0;
}
double SumFac(int x)
{
    int i = 0;
    double ret = 1.0;
    double sum = 0.0;
    for (i = 1; i <= x; i++)
    {
        ret *= i;
        sum += ret;
    }
    sum += 1.0;
    return sum;
}
double fact(int n)
{
    int i = 0;
    double ret = 1.0;
    for (i = 1; i <= n; i++)
    {
        ret *= i;
    }
    return ret;
}
int sign(int x)
{
    if (x > 0)
    {
        return 1;
    }
    else if (x == 0)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}
double dist(double x1, double y1, double x2, double y2)
{
    double a = x1 - x2;
    double b = y1 - y2;
    double c = pow(a, 2) + pow(b, 2);
    double t = sqrt(c);
    return t;
}
int sum(int m, int n)
{
    int i = 0;
    int sum = 0;
    for (i = m; i <= n; i++)
    {
        sum += i;
    }
    return sum;
}