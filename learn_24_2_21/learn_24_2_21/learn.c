#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int m = Fib(n);
//	printf("%d", m);
//	return 0;
//}
//#include<stdio.h>
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int m = Fib(n);
//	printf("%d", m);
//	return 0;
//}
//汉诺塔问题
//#include<stdio.h>
//void move(char p1, char p2)
//{
//	printf("%c->%c ", p1, p2);
//}
////n:盘子个数
////p1:起始位置
////p2:中转位置
////p3:目的位置
//void HanNuoTa(int n,char p1,char p2,char p3)
//{
//	if (n == 1)
//	{
//		move(p1, p3);
//	}
//	else
//	{
//		HanNuoTa(n - 1, p1, p3, p2);
//		move(p1, p3);
//		HanNuoTa(n - 1, p2, p1, p3);
//	}
//}
//int main()
//{
//	HanNuoTa(1, 'A', 'B', 'C');
//	printf("\n");
//	HanNuoTa(2, 'A', 'B', 'C');
//	printf("\n");
//	HanNuoTa(3, 'A', 'B', 'C');
//	printf("\n");
//	return 0;
//}
//青蛙跳台阶问题
#include<stdio.h>
int Jump(int n)
{
	if (n <= 2)
		return n;
	else
		return Jump(n - 1) + Jump(n - 2);
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int m = Jump(n);
	printf("跳%d级台阶有%d种方法",n, m);
	return 0;
}