#define _CRT_SECURE_NO_WARNINGS
//冒泡排序
//#include<stdio.h>
//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	//趟数
//	for (i = 0; i < sz - 1; i++)
//	{
//		//一趟冒泡排序
//		int flag = 1;//假设是有序的
//		int j = 0;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = 0;
//				tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//void print_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr[] = { 9,8,7,4,5,6,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz);
//	print_sort(arr, sz);
//	return 0;
//}
//二级指针
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	int** pp = &p;
//	**pp = 20;
//	printf("%d", a);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	int* p = NULL; 
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int* p = &a;
//	if (p != NULL)
//	{
//		*p = 20;
//	}
//	printf("%d", a);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	assert(p != NULL);
//	*p = 20;
//	printf("%d", a);
//	return 0;
//}
//模拟实现strlen
//#include<stdio.h>
//int my_strlen(char* arr)
//{
//	if (*arr == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(arr + 1);
//}
//#include<stdio.h>
//int my_strlen(char *str)
//{
//	int count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//	
//}
//#include<stdio.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	int count = 0;
//	assert(str);
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//
//}
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%zd", len);
//	return 0;
//}
//#include<stdio.h>
//void Swap(int* pa, int* pb)
//{
//	int z = 0;
//	z = *pa;
//	*pa = *pb;
//	*pb = z;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	Swap(&a, &b);
//	printf("%d %d", a, b);
//	return 0;
//}
//review
//#include<stdio.h>
//int main()
//{
//	int arr[3] = { 0 };
//	int i = 0;
//	while (i < 3)
//	{
//		scanf("%d", &arr[i]);
//		i++;
//	}
//	int min = arr[0];
//	i = 1;
//	while (i < 3)
//	{
//		if (arr[i] < min)
//		{
//			min = arr[i];
//			i++;
//		}
//	}
//	printf("%d", min);
//	return 0;
//}
