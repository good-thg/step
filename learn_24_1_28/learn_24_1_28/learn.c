#define _CRT_SECURE_NO_WARNINGS
//一个十六进制整数ABCDEF,对应的十进制整数是多少。
//#include<stdio.h>
//int main()
//{
//    int n = 0XABCDEF;
//    printf("%15d", n);
//    return 0;
//}
////十进制整数1234对应的八进制和十六进制（字母大写），用空格分开，
//// 并且要求，在八进制前显示前导0，在十六进制数前显示前导0X。
//#include<stdio.h>
//int main()
//{
//    int a = 1234;
//    printf("%#o %#X", a, a);
//    return 0;
//}
//
//#include<stdio.h>
//int main()
//{
//    char ch;
//    int a = 0;
//    double n = 0.0;
//    scanf("%c %d %lf", &ch, &a, &n);
//    printf("%c %d %.6lf", ch, a, n);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    scanf("%d %d %d", &a, &b, &c);
//    printf("%d%8d%8d", a, b, c);
//    return 0;
//}
//
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int m = 0;
//    int result = 0;
//    scanf("%x %o", &n, &m);
//    result = n + m;
//    printf("%d\n", result);
//    return 0;
//}
//
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%x %o", &n, &m);
//    printf("%d\n", n+m);
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int a, b;
//    while (scanf("%d %d", &a, &b) != EOF) {
//        printf("%d\n", a / b);
//    }
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int a, b;
//    while (scanf("%d %d", &a, &b) != EOF) {
//        printf("%d\n", a + b);
//    }
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int a, b;
//    while (scanf("%d %d", &a, &b) != EOF) {
//        printf("%d\n", a % b);
//    }
//    return 0;
//}
////给定一个浮点数，要求得到该浮点数的个位数。
//#include <stdio.h>
//int main() {
//    float a = 0.0;
//    while (scanf("%f", &a) != EOF) {
//        int count = (int)a;
//        printf("%d", count % 10);
//    }
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) {
//        printf("%d", a * 100);
//    }
//    return 0;
//}
//
//#include <stdio.h>
//int main() {
//    int a, b;
//    while (scanf("%d %d", &a, &b) != EOF) {
//        printf("%d %d", a / b, a % b);
//    }
//    return 0;
//}
//int Add(int x, int y)
//{
//	return x + y;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	while ((scanf("%d %d", &a, &b)) != EOF) {
//		int c = Add(a, b);
//		printf("%d\n", c);
//	}
//	return 0;
//}
//int max(int x, int y)
//{
//	int t = 0;
//	if (x > y)
//		t = x;
//	else
//		t = y;
//	return t;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	while ((scanf("%d %d", &a, &b)) != EOF) {
//		printf("%d\n", max(a, b));
//	}
//	return 0;
//}
//int max(int x, int y)
//{
//	return(x > y ? x : y);
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	while ((scanf("%d %d", &a, &b)) != EOF) {
//		printf("%d\n", max(a, b));
//	}
//	return 0;
//}
//#include <stdio.h>
//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) {
//        printf("%d\n", a % 10);
//    }
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) {
//        printf("%d\n", a / 10 % 10);
//    }
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int a, b;
//    while (scanf("%d %d", &a, &b) != EOF) {
//        printf("%d\n", (a + b - 1) % 7 + 1);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	while ((scanf("%d", &n)) != EOF) {
//		int m = n % 7;
//		printf("%d", m);
//	}
//	
//	return 0;
//}
//#include<stdio.h> 
//int main()
//{
//    int X = 0, N = 0;
//    scanf("%d%d", &X, &N);
//    int c = X + N;
//    while (c > 7)
//        c %= 7;
//    if (c == 0)
//        printf("7");
//    else
//        printf("%d", c);
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    while ((scanf("%d", &n)) != EOF) {
//        for (i = 1; i <= n; i++) {
//            for (j = 1; j <= n; j++) {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) {
//        for (int i = 1; i <= a; i++)
//        {
//            printf("Happy new year!Good luck!\n");
//        }
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    int n = 0;
//    while (scanf("%d", &n) != EOF) {
//        if (n % 400 == 0 || n % 4 == 0 && n % 100 != 0)
//        {
//            printf("yes");
//        }
//        else {
//            printf("no");
//        }
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) {
//        if (a >= 60)
//            printf("Pass\n");
//        else
//            printf("Fail\n");
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) {
//        if (a % 2 == 0)
//            printf("Even\n");
//        else
//            printf("Odd\n");
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    char ch = 0;
//    while (scanf("%c", &ch) != '\n') {
//        getchar();
//        if (ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U' || ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
//            printf("Vowel\n");
//        }
//        else {
//            printf("Consonant\n");
//        }
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    int a, b, c;
//    while (scanf("%d %d %d", &a, &b, &c) != EOF) {
//        if (a <= b && b <= c)
//            printf("true\n");
//        else
//            printf("false\n");
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    char ch = 0;
//    while (scanf("%c", &ch) != EOF) {
//        getchar();
//        if (isupper(ch))
//            printf("YES\n");
//        else
//            printf("NO\n");
//    }
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    char ch = 0;
//    while (scanf("%c", &ch) != EOF) {
//        getchar();
//        if (isalpha(ch))
//            printf("YES\n");
//        else
//            printf("NO\n");
//    }
//    return 0;
//}