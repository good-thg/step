﻿#define _CRT_SECURE_NO_WARNINGS


//#include <stdio.h>
//int main() {
//    int i = 0;
//    int j = 0;
//    for (i = 1; i <= 9; i++) {
//        for (j = 1; j <= i; j++) {
//            printf("%d*%d=%2d ", j, i, i * j);
//        }
//        printf("\n");
//    }
//    return 0;
//}


//  预定义符号
//  __FILE__ //进⾏编译的源⽂件
//  __LINE__ //⽂件当前的⾏号
//  __DATE__ //⽂件被编译的⽇期
//  __TIME__ //⽂件被编译的时间
//  __STDC__ //如果编译器遵循ANSI C，其值为1，否则未定义
//


//#include<stdio.h>
//int main()
//{
//	printf("%s %d", __FILE__, __LINE__);
//	printf("%s %s", __DATE__,__TIME__);
//	return 0;
//}


//#define Max 100
//#include<stdio.h>
//int main()
//{
//	printf("%d", Max);
//	return 0;
//}

//#define SQUARE(x) x*x
//#include<stdio.h>
//int main()
//{
//	int n = 5;
//	int ret = SQUARE(n);
//	printf("%d\n", ret);
//	return 0;
//}

#define MAX(A,B) A>B?A:B
#include<stdio.h>
int main()
{
	int a = 3;
	int b = 5;
	int m = MAX(a++, b++);
	printf("%d\n", m);
	printf("%d\n", a);
	printf("%d\n", b);
	return 0;
}